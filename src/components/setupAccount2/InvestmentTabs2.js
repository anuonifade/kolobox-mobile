import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import {
  createNavigator,
  createNavigationContainer,
  TabRouter,
  addNavigationHelpers
} from 'react-navigation';
import SetupAccount2ShortlistInvestment from './SetupAccount2ShortlistInvestment';
import SetupAccount2AccountDetail from './SetupAccount2AccountDetail';

const CustomTabBar = ({ navigation }) => {
  const { routes } = navigation.state;
  return (
    <View style={styles.tabContainer}>
      {routes.map(route => (
        <TouchableOpacity
          onPress={() => navigation.navigate(route.routeName)}
          style={styles.tab}
          key={route.routeName}
        >
          <Text>{route.routeName}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const CustomTabView = ({ router, navigation, product }) => {
  const { routes, index } = navigation.state;
  const ActiveScreen = router.getComponentForState(navigation.state);
  return (
    <View style={styles.container}>
      <CustomTabBar navigation={navigation} />
      <ActiveScreen
        navigation={addNavigationHelpers({
          ...navigation,
          state: routes[index]
        })}
        product={product}
      />
    </View>
  );
};

const CustomTabRouter = TabRouter(
  {
    Shortlisted: {
      screen: SetupAccount2ShortlistInvestment,
      path: ''
    },
    'Account Details': {
      screen: SetupAccount2AccountDetail,
      path: ''
    }
  },
  {
    initialRouteName: 'Shortlisted'
  }
);

const InvestmentTabs2 = createNavigationContainer(
  createNavigator(CustomTabRouter)(CustomTabView)
);

const styles = StyleSheet.create({
  container: {
    flex: 1
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  tabContainer: {
    flexDirection: 'row',
    height: 48
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 4,
    borderWidth: 1,
    borderColor: '#ddd',
    backgroundColor: 'white'
  }
});

export default InvestmentTabs2;
