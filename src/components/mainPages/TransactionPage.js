import React, { Component } from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import { Actions } from 'react-native-router-flux';
import { AccountHeader } from '../common';
import TransactionTabs from './TransactionTabs';

class TransactionPage extends Component {
  static navigationOptions = {
    tabBarLabel: 'Earnings',
    tabBarIcon: () => (<Icon size={24} color="white" name="attach-money" />)
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
      <AccountHeader showHelp headerTxt={'Kolonaija'} />
      <TransactionTabs />

      </View>
    );
  }
}

export default TransactionPage;
