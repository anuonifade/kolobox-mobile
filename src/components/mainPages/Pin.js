// {This file is used to update user pin.}
import React, { Component } from 'react'
import { View, Text, TextInput, TouchableOpacity, Keyboard, Alert, AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Spinner } from '../common'

export default class PIN extends Component {
  constructor (props) {
    super(props)
    this.data = null
    this.state = { text1: '', text2: '', text3: '', text4: '', updating: false }
  }
  // this method contain Api to update user pin.
  updatePin () {
    if (this.state.text1 === '' || this.state.text2 === '' ||
      this.state.text3 === '' || this.state.text4 === '') {
      Alert.alert(
        'Error!',
        'Please Fill all the fields',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') }
        ],
        { cancelable: false }
      )
      return
    }

    AsyncStorage.getItem('userData', (err, result) => {
      this.data = JSON.parse(result)
      this.setState({ updating: true })
      fetch(GLOBALS.BASE_URL + 'user/me/pin', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.data.token
        },
        body: JSON.stringify({
          pin: this.state.text1 + this.state.text2 + this.state.text3 + this.state.text4
        })
      }).then((response) => response.json())
        .then(responseJson => {
          if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
            AsyncStorage.removeItem('userData', (error) => {
              if (error !== null) {
                console.log('unable to log out')
                console.log(err)
              }
              Actions.loginPages()
            })
          } else {
            this.setState({ updating: false })
            if (responseJson.status === true) {
              Alert.alert(
                'Success',
                responseJson.data,
                [
                  { text: 'OK' }
                ],
                { cancelable: false }
              )
            } else {
              Alert.alert(
                'Error!',
                responseJson.message,
                [
                  { text: 'OK' }
                ],
                { cancelable: false }
              )
            }
          }
        })
        .catch((error) => {
          console.error(error)
        })
    })
  }
  // This method is used for to show activity indication during pin update.
  renderButton () {
    if (!this.state.updating) {
      return (
        <TouchableOpacity onPress={this.updatePin.bind(this)} style={{ alignItems: 'center' }}>
          <Text style={Styles.ForgotText}>
            Change
          </Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <Spinner />
      )
    }
  }
  // This method is used to show all views.
  render () {
    const { cover } = Styles
    return (
      <View style={cover}>
        <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
          <View style={Styles.textInputContainerStyle}>
            <TextInput
              autoFocus
              style={Styles.textInputStyle}
              onChangeText={(text1) => {
                this.setState({ text1 })
                if (text1 && text1.length === 1) {
                  this.refs.SecondInput.focus()
                }
              }}
              value={this.state.text1}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.SecondInput.focus()
              }} />
            <TextInput
              ref='SecondInput'
              style={Styles.textInputStyle}
              onChangeText={(text2) => {
                this.setState({ text2 })
                if (text2 && text2.length === 1) {
                  this.refs.ThirdInput.focus()
                }
              }}
              value={this.state.text2}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.ThirdInput.focus()
              }} />
            <TextInput
              ref='ThirdInput'
              style={Styles.textInputStyle}
              onChangeText={(text3) => {
                this.setState({ text3 })
                if (text3 && text3.length === 1) {
                  this.refs.FourthInput.focus()
                }
              }}
              value={this.state.text3}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.FourthInput.focus()
              }} />
            <TextInput
              ref='FourthInput'
              style={Styles.textInputStyle}
              onChangeText={(text4) => {
                this.setState({ text4 })
                if (text4 && text4.length === 1) {
                  Keyboard.dismiss()
                }
              }}
              value={this.state.text4}
              keyboardType='numeric'
              maxLength={1} />
          </View>
          {this.renderButton()}
        </View>
      </View>
    )
  }
}
const Styles = {
  textInputContainerStyle: {
    flexDirection: 'row',
    marginTop: 10
  },
  textInputStyle: {
    height: 100,
    width: 80,
    borderColor: 'gray',
    borderWidth: 2,
    margin: 5,
    flex: 2,
    fontSize: 30,
    alignSelf: 'center',
  },
  blackTextStyles: {
    fontSize: 16,
    color: '#3d93cb',
    borderBottomWidth: 2,
    borderColor: '#3d93cb'
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  }
}
