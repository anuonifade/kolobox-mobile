import React, { Component } from 'react';
import { ScrollView, View, Alert, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';

import { Spinner } from '../common/spinner';
import { AccountHeader } from '../common';
import NameOfInvestmentDetail from './NameOfInvestmentDetail';

import {
  getInvestmentApproach,
  getMyInvestmentApproach,
  addToShortlist,
  removefromShortlist
} from '../../actions/investment-actions';

class InvestmentList extends Component {
  static navigationOptions = {
    tabBarLabel: 'New',
    tabBarIcon: () => (<Icon size={24} color='white' name='add' />)
  }

  constructor(props) {
    super(props);
    this.data = null;
    this.state = {
      loading: false,
      investment: {
        ProductCategories: [],
        shortlisted: {
          product: []
        }
      }
    };
  }

  componentWillMount() {
    const { getMyInvestmentApproach } = this.props;

    getMyInvestmentApproach()
    .then(investment => this.loadInvestmentOptions(investment));
  }

  loadInvestmentOptions(investment) {
    const { getInvestmentApproach, token } = this.props;
    const hasInvestment = !!investment;
    if (!hasInvestment) {
      return;
    }

    this.setState({ loading: true });
    getInvestmentApproach(token, investment.id)
    .then(({ status, data, message }) => {
      if (!status) {
        Alert.alert('Error', message, [{ text: 'Try again',
          onPress: () => {
            this.setState({ loading: false });
            this.loadInvestmentOptions(investment.id);
          } }]);
        return;
      }
      this.setState({
        investment: data,
        loading: false
      });
    });
  }

  handleCheckToggle(product, checked) {
    const { addToShortlist, removefromShortlist } = this.props;
    if (!checked) {
      removefromShortlist(product.id);
    } else {
      addToShortlist(product);
    }
  }

  showInvestmentProducts(product, index) {
    const { shortlisted } = this.props;
    const checked = !!shortlisted[product.id];
    return (
      <NameOfInvestmentDetail
        checked={checked}
        product={product}
        key={index}
        handleCheckToggle={this.handleCheckToggle.bind(this)}
      />
    );
  }

  renderInvestmentView() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        {this.renderRadio()}
      </View>
    );
  }

  render() {
    const { shortlisted } = this.props;
    const styledSpinner = <View style={styles.styledSpinner}><Spinner /></View>;

    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <AccountHeader showHelp showWallet headerTxt={'Kolobox'} />
        <ScrollView>
          <View style={{ flex: 1, top: 10, alignItems: 'center', justifyContent: 'center' }} >
            <TouchableOpacity
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                paddingRight: 10,
                paddingLeft: 10,
                height: 30,
                backgroundColor: '#60a3bc'
              }}
            >
              <Text
                style={{
                  color: '#ffffff',
                  fontWeight: 'bold',
                  alignSelf: 'center',
                  justifyContent: 'center'
                }}
                onPress={() => {
                  AsyncStorage.removeItem('InvestmentApproach');
                  Actions.InvestmentPages();
                }}
              >ADD NEW INVESTMENT</Text>
            </TouchableOpacity>
            <View style={styles.renderRadio}>
              <Text style={{ fontSize: 16 }}> You have not selected any product.</Text>
              <Text
                style={{ fontSize: 12, color: '#3B90E3', top: 10 }}
                onPress={() => Actions.InvestmentPages()}
              >
                CLICK HERE TO ADD NEW PRODUCT
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

// const mapStateToProps = (
//   { user: { userData: { token } }, investment: { shortlisted } }
// ) => ({ token, shortlisted });

const mapStateToProps = (state) => {
  const { token } = state.user;
  const { shortlisted } = state.investment;
  return {
    token,
    shortlisted
  };
};

const mapDispatchToProps = ({
  getInvestmentApproach,
  getMyInvestmentApproach,
  addToShortlist,
  removefromShortlist
});

const styles = {
  styledSpinner: {
    marginTop: 50,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  renderRadio: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(InvestmentList);
