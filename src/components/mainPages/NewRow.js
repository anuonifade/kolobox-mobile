import React, { Component } from 'react';
import { Text, View } from 'react-native';


 class NewRow extends Component {


   render() {
   return (

  <View style={styles.containerStyle}>
     <View style={styles.childContainerStyle}>
         <Text style={styles.textStyles}>Name of Investment</Text>
     </View>
     <View style={styles.interestContainerStyle}>
        <View style={styles.leftViewStyle}>
         <Text style={styles.blackTextStyles}>INTEREST</Text>
         <Text style={styles.blackTextStyles}>4.59%-9.89%</Text>
         </View>
         <View style={styles.rightViewStyle}>
             <Text style={styles.blackTextStyles}>Month</Text>
             <Text style={styles.blackTextStyles}>Compounding</Text>
             </View>

     </View>

     <View style={styles.accountContainerStyle}>
          <View style={styles.leftViewStyle}>
         <Text style={styles.blackTextStyles}>Amount</Text>
         <Text style={styles.blackTextStyles}>Upto 200'k'</Text>
         </View>
        <View style={styles.rightViewStyle}>
             <Text style={styles.blackTextStyles}>Month</Text>
             <Text style={styles.blackTextStyles}>1 to 5 Years</Text>
             </View>

     </View>

 </View>

   );
 }
 }

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    borderColor: '#4991bf',
    borderWidth: 2,
    margin: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#4991bf',
    padding: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
   textStyles: {
    fontSize: 16,
    color: '#ffffff',
    padding: 5,
    flex: 1,
    justifyContent: 'flex-start',
  },
  blackTextStyles: {
   fontSize: 16,
   color: '#000000',
   padding: 5,
 },
  interestContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#4991bf',
      padding: 5
  },
  accountContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
      padding: 5
  },
  rightViewStyle: {
        flex: 1,
      alignItems: 'flex-end',
      justifyContent: 'flex-end'
  },
  leftViewStyle: {
      flex: 1,
        alignItems: 'flex-start',
      justifyContent: 'flex-start'
  },
  arrowImageView: {

      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      flexDirection: 'row'
  }
};

export default NewRow;
