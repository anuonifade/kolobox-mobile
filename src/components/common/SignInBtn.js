import React from 'react'
import { Text, TouchableHighlight } from 'react-native'

const SignInBtn = ({ onPress, children }) => {
  const { btn } = Styles
  return (
    <TouchableHighlight onPress={onPress} style={btn}>
      <Text>{children}</Text>
    </TouchableHighlight>
  )
}

const Styles = {
  btn: {
    borderWidth: 1,
    borderColor: '#000',
    height: 30

  }
}

export { SignInBtn }
