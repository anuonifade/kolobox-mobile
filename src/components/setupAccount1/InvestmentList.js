import React, { Component } from 'react';
import { ScrollView, View, Alert, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';

import { Spinner } from '../common/spinner';
// import NameOfInvestmentDetail from './NameOfInvestmentDetail';
// import ProductCategoryInfo from './ProductCategoryInfo';
import renderIf from '../common/renderIf';
import ShortlistInvestment from './ShortlistInvestment';
import {
  getInvestmentApproach,
  getMyInvestmentApproach,
  addToShortlist,
  removefromShortlist
} from '../../actions/investment-actions';

class InvestmentList extends Component {
  constructor(props) {
    super(props);
    this.data = null;
    this.state = {
      loading: true,
      investment: {
        ProductCategories: [],
        shortlisted: {}
      },
      status: false,
      modalVisible: false
    };
    this.modalVisible = this.modalVisible.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.fetchListedProducts = this.fetchListedProducts.bind(this);
  }

  componentWillMount() {
    const { getMyInvestmentApproach } = this.props;

    getMyInvestmentApproach()
    .then(investment => this.loadInvestmentOptions(investment))
    .catch((error) => {
      Alert.alert(error);
    });
  }

  loadInvestmentOptions(investment) {
    const { getInvestmentApproach, token } = this.props;
    getInvestmentApproach(token, investment.id)
    .then(({ status, data, message }) => {
      if (!status) {
        Alert.alert('Error', message, [{ text: 'Try again',
          onPress: () => {
            this.setState({ loading: false });
            this.loadInvestmentOptions(investment.id);
          } }]);
        return;
      }
      this.setState({
        investment: data,
        loading: false
      });
    });
  }

  fetchListedProducts(category) {
    const { addToShortlist, removefromShortlist } = this.props;
    addToShortlist(category);
    this.setState({
      status: !this.state.status
    });
  }

  modalVisible(visible = true) {
    this.setState({
      modalVisible: visible
    });
  }

  closeModal(visible = false) {
    this.setState({
      modalVisible: visible
    });
  }

  renderProductList(category, index) {
    return (
      <View style={{ marginTop: 20 }}>
        <TouchableOpacity
          onPress={() => this.fetchListedProducts(category)}
          style={{ marginTop: 5, backgroundColor: '#4991bf' }}
        >
          <View style={Styles.benefitsContainerStyle}>
            <Text style={Styles.debitCardTextStyle}>{category.name.toUpperCase()}</Text>
            <Image style={Styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
          </View>
          <View style={{ padding: 0, flexDirection: 'row' }}>
            <Text style={[Styles.lineStyle, { backgroundColor: '#fff', paddingTop: 0 }]} />
          </View>
        </TouchableOpacity>
        {renderIf(this.state.status)(
          !this.state.loading
          ? <ShortlistInvestment />
          : <View
              style={{
                  marginTop: 50,
                  flex: 1,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
          >
            <Spinner />
          </View>
        )}
      </View>
    );
  }


  renderRadio() {
    const { investment } = this.state;
    const emptyView = (
      <View
        style={{
          marginTop: 50,
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Text> No product available in this category. </Text>
      </View>
    );
    const investmentView = [];
    investment
      .ProductCategories
        .forEach((category, index) =>
            investmentView.push(this.renderProductList(category, index))
        );
    return investmentView.length === 0 ? emptyView : investmentView;
  }

  renderInvestmentView() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        {this.renderRadio()}
      </View>
    );
  }

  render() {
    const styledSpinner = (
      <View
        style={{
            marginTop: 50,
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
          }}
      >
        <Spinner />
      </View>
    );
    const { investment } = this.state;
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              {this.state.loading ? styledSpinner : this.renderRadio()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const Styles = {
  textStyles: {
    fontSize: 16,
    color: '#000000',
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 10,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  benefitTextStyle: {
    fontSize: 16,
    color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bulletStyle: {
    fontSize: 27,
    color: '#000000',
  },
  bulletTextStyle: {
    fontSize: 16,
    color: '#000000',
    marginLeft: 5,
    marginRight: 5,
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    padding: 5,
    alignSelf: 'flex-end'
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#fff',
    flex: 1
  }
};

const mapStateToProps = (state) => {
  const { token } = state.user;
  const { shortlisted } = state.investment;
  return {
    token,
    shortlisted
  };
};

const mapDispatchToProps = ({
  getInvestmentApproach,
  getMyInvestmentApproach,
  addToShortlist,
  removefromShortlist
});

export default connect(mapStateToProps, mapDispatchToProps)(InvestmentList);
