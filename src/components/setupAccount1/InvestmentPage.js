import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { AccountHeader } from '../common';
import SelectProductTabs from './SelectProductTabs';

class InvestmentPage extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AccountHeader showHelp showSkip headerTxt={'Kolobox'} />
        <View style={Styles.imageView} >
          <Image style={Styles.HeaderImage} source={require('../img/hopskotch.png')} />
          <Text style={Styles.textStyles} >Shortlist investments you are interested in</Text>
        </View>
        <SelectProductTabs />
      </View>
    );
  }
}

const Styles = {
  HeaderImage: {
    height: 25,
    width: 25,
    padding: 10
  },
  imageView: {
    justifyContent: 'center',
    height: 40,
    flexDirection: 'row',
    margin: 5
  },
  textStyles: {
    fontSize: 14,
    color: '#000000',
    padding: 5,
  }
};
export default InvestmentPage;
