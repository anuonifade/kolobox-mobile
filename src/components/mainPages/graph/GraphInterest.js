import React, { Component } from 'react';
import { View, ScrollView, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import SavingsBarChart from './graph';
import GraphDetails from './GraphDetails';
import { getUserDashboard } from '../../../actions/user-actions';

class GraphInterest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: '',
      totalInterest: 0.000,
      averageInterest: 0.000,
      data: [],
      keys: [],
      investment: [],
      chartStart: -5,
      chartEnd: undefined,
      dashboard: {},
    };
    this.renderChartData = this.renderChartData.bind(this);
    this.loadDashboard = this.loadDashboard.bind(this);
    this.plotChart = this.plotChart.bind(this);
    this.startOfChart = this.startOfChart.bind(this);
    this.endOfChart = this.endOfChart.bind(this);
  }

  componentWillMount() {
    const { getUserDashboard, userData } = this.props;
    this.setState({ loading: true });

    getUserDashboard(userData.token)
      .then((dashboard) => {
        this.setState({ dashboard: dashboard.data });
        this.loadDashboard(dashboard.data);
      }).catch(() => {
        this.setState({
          error: 'No Savings or Interest to load',
          loading: false
        });
      });
  }

  loadDashboard(dashboard) {
    let totalInterest = 0;
    let averageInterest = 0;

    dashboard.interest.data.forEach((perInterest) => {
      totalInterest += parseFloat(parseFloat(perInterest.total_interest).toFixed(2));
    });
    averageInterest = totalInterest / dashboard.interest.data.length;
    this.setState({
      totalInterest: parseFloat(totalInterest).toFixed(2),
      averageInterest: parseFloat(averageInterest).toFixed(2),
      data: this.renderChartData(dashboard),
    });
  }

  plotChart() {
    if (this.state.error && this.state.error !== '') {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
          <Text style={{ top: 10, fontWeight: 'bold' }} >
            { this.state.error }
          </Text>
        </View>
      );
    }

    return (
      <SavingsBarChart chart={this.state.data} keys={this.state.keys} />
    );
  }

  startOfChart() {
    this.setState({
      loading: true,
      chartStart: 0,
      chartEnd: 5,
    });
    this.loadDashboard(this.state.dashboard);
    this.plotChart();
  }

  endOfChart() {
    this.setState({
      loading: true,
      chartStart: -5,
      chartEnd: undefined,
    });
    this.loadDashboard(this.state.dashboard);
    this.plotChart();
  }

  renderChartData(dashboard) {
    const responses = [];
    const uniqueKeys = [];
    if (dashboard.interest.chart && dashboard.interest.chart.length > 0) {
      dashboard
        .interest
          .chart
            .map((interest, index) => {
                if (responses.find((x) =>
                  x.date.getTime() === (new Date(interest.date)).getTime())) {
                  responses
                    .find(x => x.date.getTime() === (new Date(interest.date))
                    .getTime())[interest.name] = parseFloat(interest.amount || 0).toFixed(2);
                } else {
                  const obj = {};
                  obj.date = new Date(interest.date);
                  obj[interest.name] = parseFloat(interest.amount || 0).toFixed(2);
                  responses.push(obj);
                }
                uniqueKeys.indexOf(interest.name) === -1
                ? uniqueKeys.push(interest.name)
                : console.log('Key already exist');
            });
    }

    this.setState({
      keys: uniqueKeys,
      investment: uniqueKeys,
      loading: false,
    });

    responses.sort((a, b) => {
      const keyA = new Date(a.date);
      const keyB = new Date(b.date);
      // Compare the 2 dates
      if (keyA < keyB) return -1;
      if (keyA > keyB) return 1;
      return 0;
    });
    return responses.slice(this.state.chartStart, this.state.chartEnd);
  }

  render() {
    // const { totalInterest, averageInterest } = this.props.dashboard;
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <View>
          <Text
            style={{
              justifyContent: 'flex-end',
              alignSelf: 'flex-end',
              margin: 3
            }}
          >*Interest Projected</Text>
        </View>
        <ScrollView style={{ top: 20 }}>
          {
            this.state.loading
            ? <ActivityIndicator size="large" color="#1034A6" />
            : this.plotChart()
          }
          {
            this.state.error && this.state.error !== ''
            ? <View />
            : <View style={{ top: 20 }}>
                <GraphDetails
                  investment={this.state.investment}
                  labelTotal="Total Interest"
                  labelAverage="Average Interest"
                  total={this.state.totalInterest}
                  average={this.state.averageInterest}
                />
              </View>
          }
        </ScrollView>
      </View>

    );
  }
}
// Styles used for View adjustment

const Styles = {
  buttonStyle: {
    height: 40,
    width: 170,
    marginBottom: 5,
    marginTop: 10,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb',
    paddingTop: 10,
    paddingBottom: 10,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
  }
};


const mapStateToProps = (state) => {
  const { userData, dashboard } = state.user;
  return {
    userData,
    dashboard
  };
};

const mapDispatchToProps = ({
  getUserDashboard
});

export default connect(mapStateToProps, mapDispatchToProps)(GraphInterest);
