import React, { Component } from 'react';
import { View, Text, ScrollView, Alert } from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { AccountHeader, FullWidthButton } from '../common';
import { Spinner } from '../common/spinner';
import { getInvestmentApproach, setMyInvestmentApproach } from '../../actions/investment-actions';

class InvestmentApproach extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      investments: [],
      investmentId: -1
    };
  }

  componentWillMount() {
    this.loadInvestmentApproach();
  }

  onClick() {
    const { investmentId, investments } = this.state;
    const { setMyInvestmentApproach } = this.props;
    let myInvestment = {};
    if (investmentId === -1) {
      Alert.alert('Alert!', 'Please select an approach first', 'OK', { cancellable: false });
      return;
    }
    investments.forEach(investment => {
      if (investmentId === investment.id) {
        myInvestment = investment;
      }
    });
    setMyInvestmentApproach(myInvestment)
    .then(() => Actions.InvestmentPage());
  }

  onSelect(index, value) {
    this.setState({
      investmentId: value
    });
  }

  getRadio(invest) {
    return (
      <RadioButton value={invest.id} key={invest.id}>
        <Text>
          {invest.name}
        </Text>
      </RadioButton>
    );
  }

  loadInvestmentApproach() {
    const { getInvestmentApproach, token } = this.props;
    getInvestmentApproach(token, '')
    .then(({ status, data, message }) => {
      if (!status) {
        Alert.alert('Error', message, [{ text: 'Try again',
          onPress: () => {
            this.setState({ loading: false });
            this.loadInvestmentApproach();
          } }]);
        return;
      }
      this.setState({
        investments: data,
        loading: false
      });
    });
  }

  renderRadio() {
    const { investments } = this.state;
    const radioButtons = [];
    investments.forEach(investment => radioButtons.push(this.getRadio(investment)));

    return radioButtons;
  }

  renderInvestmentView() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }} key={this.state.investments.id}>
        <Text style={Styles.textStyles}>
          Select an Investment approach
        </Text>
        <View style={Styles.containerRAdio}>
          <RadioGroup
            style={Styles.containerRAdio}
            onSelect={(index, value) => this.onSelect(index, value)} selectedIndex={-1}
          >
            {this.renderRadio()}
          </RadioGroup>
        </View>
      </View>
    );
  }

  renderView() {
    const { loading } = this.state;

    if (loading) {
      return <Spinner />;
    }

    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          {this.renderInvestmentView()}
        </View>
      </ScrollView>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <AccountHeader showHelp showSkip headerTxt={'Kolobox'} />
        {this.renderView()}
        <FullWidthButton onPress={this.onClick.bind(this)} />
      </View>
    );
  }
}
const Styles = {
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },
  forgotButton: {
    flex: 1,
    alignItems: 'center'
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  },
  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center'
  },
  InputStyle: {
    marginTop: 12,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0,
    height: 25,
    borderBottomWidth: 2,
    borderColor: '#D3D3D3'
  },
  imageView: {
    height: 40,
    flexDirection: 'row',
    margin: 5
  },
  textStyles: {
    fontSize: 14,
    color: '#000000',
    padding: 5,
    marginTop: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    marginTop: 5
  }
};

const mapStateToProps = ({ user: { userData: { token } } }) => ({ token });

const mapDispatchToProps = ({
  getInvestmentApproach,
  setMyInvestmentApproach
});

export default connect(mapStateToProps, mapDispatchToProps)(InvestmentApproach);
