import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import YearSelector from '../common/yearSelector.js';
import MonthSelector from '../common/monthSelector.js';
import SwitchBtn from '../common/switch.js';


export default class DebitCardAdd extends Component {
  constructor() {
    super();
    this.state = {
      switch1Value: false,
      switch2Value: false,
    };
  }

  toggleSwitch1 = (value) => {
    this.setState({ switch1Value: value });
  }
  toggleSwitch2 = (value) => {
    this.setState({ switch2Value: value });
  }

  render() {
    const { cover, forgotButton, ForgotText } = Styles;
    return (
      <View style={cover}>
        <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Icon
              name={'credit-card'}
              size={25}
              color='grey'
              style={{ padding: 2, height: 25, marginTop: 5 }}
            />
            <TextInput
              underlineColorAndroid='transparent'
              autoCorrect={false}
              placeholder='Card Number'
              placeholderTextColor={'#D3D3D3'}
              style={[Styles.InputStyle, { flex: 1 }]}
              keyboardType={'numeric'}
            />
            <Icon
              name={'cc-mastercard'}
              size={25}
              color='blue'
              style={{ padding: 2, height: 25, marginTop: 5 }}
            />
          </View>
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <MonthSelector />
          <YearSelector />
        </View>
        <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Icon
              name={'credit-card'}
              size={25} color='grey'
              style={{ padding: 2, height: 25, marginTop: 5 }}
            />
            <TextInput
              underlineColorAndroid='transparent'
              autoCorrect={false}
              placeholder='CVV'
              placeholderTextColor={'#D3D3D3'}
              style={Styles.InputStyle}
              keyboardType={'numeric'}
            />
          </View>
        </View>
        {
          this.props.renderAddCard
          ? (
              <View style={forgotButton}>
                <TouchableOpacity>
                  <Text style={ForgotText} >Add Card</Text>
                </TouchableOpacity>
              </View>
            )
          : <View />
        }
      </View>
    );
  }
}
const Styles = {
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },
  forgotButton: {
    flex: 1,
    alignItems: 'center',
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline',
  },
  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center',
  },
  InputStyle: {
    marginTop: 12,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0,
    height: 25,
    borderBottomWidth: 2,
    borderColor: '#D3D3D3'
  }
};
