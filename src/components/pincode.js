// {This file is used to enter pin to navigate into next screen.}
import React, { Component } from 'react';
import { Text, View, TextInput, Keyboard, Alert, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { AccountHeader, FullWidthButton, Spinner } from './common';
import { verifyPin, resendPin } from '../actions/user-actions';


class Pincode extends Component {
  constructor(props) {
    super(props);
    this.data = null;
    this.state = {
      text1: '',
      text2: '',
      text3: '',
      text4: '',
      loading: false
    };
  }

  verifyingPin() {
    if (this.state.text1 === '' || this.state.text2 === '' ||
      this.state.text3 === '' || this.state.text4 === '') {
      Alert.alert(
        'Error!',
        'Please Fill all the fields',
        [
          { text: 'OK' }
        ],
        { cancelable: false }
      );
      return;
    }
    const { text1, text2, text3, text4 } = this.state;
    const code = `${text1}${text2}${text3}${text4}`;
    const { token, verifyPin } = this.props;
    this.setState({ loading: true })

    verifyPin(token, code)
      .then(responseData => {
        const { status, message } = responseData;
        this.setState({ loading: false })

        if (!status) {
          Alert.alert(
            'Error',
            message,
            [
              { text: 'OK', onPress: () => {} } // go to login page
            ],
            { cancelable: false }
          );
          return;
        }

        Alert.alert(
          'Email Verified',
          'Press ok to create new pin for verifying my account.',
          [
            { text: 'OK', onPress: () => Actions.changePin() } // go to change pin page
          ],
          { cancelable: false }
        )
      })
      .catch(error => {
        this.setState({ loading: false })
        Alert.alert(
          'Error',
          error.message,
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )

        throw error;
      })
  }

  resendCodeApi() {
    const { token, userData: { email }, resendPin } = this.props;
    this.setState({ loading: true })

    resendPin(token, email)
      .then(responseData => {
        const { status } = responseData
        this.setState({ loading: false })

        if (!status) {
          Alert.alert(
            'Error',
            'Token expired, kindly login and try again',
            [
              { text: 'OK', onPress: () => Actions.loginPages() } // go to login page
            ],
            { cancelable: false }
          )
          return
        }
        Alert.alert(
          'Success',
          'New code sent to your email',
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )
      })
      .catch(error => {
        this.setState({ loading: false })
        Alert.alert(
          'Error',
          error.message,
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )

        throw error
      })
  }

  renderCode() {
    if (this.state.loading) {
      return <Spinner />
    }
    return (
      <TouchableOpacity onPress={this.resendCodeApi.bind(this)}>
        <Text style={Styles.ForgotText}> Resend Code </Text>
      </TouchableOpacity>
    )
  }

  renderButton() {
    if (this.state.loading) {
      return <Spinner />
    }
    return <FullWidthButton onPress={this.verifyingPin.bind(this)} />
  }

  // This render method used for rendering UI.
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <AccountHeader headerTxt='Kolobox' showHelp />
        <View style={Styles.componentStyle}>
          <Text style={Styles.textStyles}>
            Welcome {this.props.userData.firstname},
            Please Enter the verification code sent to your mail.
          </Text>
          <View style={Styles.textInputContainerStyle}>
            <TextInput
              autoFocus
              style={Styles.textInputStyle}
              onChangeText={text1 => {
                this.setState({ text1 })
                if (text1 && text1.length === 1) {
                  this.refs.SecondInput.focus()
                }
              }}
              value={this.state.text1}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={event => {
                this.refs.SecondInput.focus()
              }}
            />
            <TextInput
              ref='SecondInput'
              style={Styles.textInputStyle}
              onChangeText={(text2) => {
                this.setState({ text2 })
                if (text2 && text2.length === 1) {
                  this.refs.ThirdInput.focus()
                }
              }}
              value={this.state.text2}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.ThirdInput.focus()
              }}
            />
            <TextInput
              ref='ThirdInput'
              style={Styles.textInputStyle}
              onChangeText={(text3) => {
                this.setState({ text3 })
                if (text3 && text3.length === 1) {
                  this.refs.FourthInput.focus()
                }
              }}
              value={this.state.text3}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.FourthInput.focus()
              }}
            />
            <TextInput
              ref='FourthInput'
              style={Styles.textInputStyle}
              onChangeText={text4 => {
                this.setState({ text4 })
                if (text4 && text4.length === 1) {
                  Keyboard.dismiss()
                }
              }}
              value={this.state.text4}
              keyboardType='numeric'
              maxLength={1}
            />
          </View>
          <View style={{ alignItems: 'center' }}>
            {this.renderCode()}
          </View>
          {this.renderButton()}
        </View>
      </View>
    )
  }
}

// Style components

const Styles = {
  textStyles: {
    fontSize: 20,
    color: '#000000',
    marginTop: 20
  },
  componentStyle: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',

    flexDirection: 'column',
    padding: 1,
    flex: 1

  },
  textInputContainerStyle: {
    flexDirection: 'row',
    marginTop: 10
  },
  textInputStyle: {
    height: 100,
    width: 80,
    borderColor: '#ddd',
    borderWidth: 2,
    margin: 5,
    flex: 2,
    fontSize: 30,
    alignSelf: 'center',
  },
  buttonStyle: {
    height: 40,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#4991bf',
    borderColor: '#4991bf'
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#ffffff',
    fontWeight: '600',
    paddingTop: 10
  }
}

const mapStateToProps = ({ user: { userData, token } }) => ({ userData, token });

const mapDispatchToProps = ({
  verifyPin,
  resendPin
});

export default connect(mapStateToProps, mapDispatchToProps)(Pincode);
