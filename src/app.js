import React, { Component } from 'react';
import { Platform, View, ActivityIndicator,  } from 'react-native';
import Expo, { Font } from 'expo';

import { Provider } from 'react-redux';

import { store } from './store';
import Router from './router';
import rootReducer from './reducers';
import { SECRET_KEY, PUBLIC_KEY } from './api/secret-key';



GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

class AuthApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false
    }
    this.defaultFonts = this.defaultFonts.bind(this);
  }

  

  componentWillMount() {
    Expo.SecureStore.setItemAsync('PAYSTACK_SECRET_KEY', SECRET_KEY);
    Expo.SecureStore.setItemAsync('PAYSTACK_PUBLIC_KEY', PUBLIC_KEY);

    Font.loadAsync({
      'Roboto': require('../assets/fonts/Roboto.ttf')
    })
      .then(() => {
        this.setState({ fontLoaded: true });
        this.defaultFonts();
      }).catch((error) => {
        console.log('ERROR LOADING FONT >>>> ', error);
      });
  }

  defaultFonts(){
    const customTextProps = {
      style: {
        fontFamily: 'Roboto'
      }
    }
    setCustomText(customTextProps)
  }

  render () {
    return (
        this.state.fontLoaded 
        ? <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 26 : 0 }}>
            <Router />
          </View>
        : <View>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
    )
  }
}

const ReduxApp = () => (
  <Provider store={store}>
    <AuthApp />
  </Provider>
)

export default ReduxApp
