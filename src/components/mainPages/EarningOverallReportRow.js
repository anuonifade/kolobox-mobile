//This file used for Earning overall report row
import React, { Component } from 'react';
import { View, Text, Alert } from 'react-native';
import Moment from 'moment';

class EarningOverallReportRow extends Component {

  getInvestmentName = (investmentName) => {
    Alert.alert(investmentName);
  }

  render() {
    Moment.locale('en');

    return (
      <View>
        <Text
          style={[
            styles.lineStyle,
            {
              backgroundColor: '#000000',
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              paddingTop: 0
            }
          ]}
        />
        <View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5, alignItems: 'center' }}>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text
              style={[
                styles.textStyles,
                {
                  color: '#000000',
                  padding: 0
                }
              ]}
            > { Moment(this.props.data.date).format('Do MMM YY') } </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text
              style={[
                styles.textStyles,
                {
                  color: '#000000',
                  padding: 0
                }
              ]}
              onPress={() => Alert.alert(this.props.data.name)}
            > { this.props.data.name.substring(0, 5) }... </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text
              style={[
                styles.textStyles,
                {
                  color: '#000000',
                  padding: 0
                }
              ]}
            > 
            { 
              parseFloat(this.props.data.amount || 0)
              .toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') 
            } 
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text
              style={[
                styles.textStyles,
                {
                  color: '#000000',
                  padding: 0
                }
              ]}
            > { 
                parseFloat(this.props.data.interest || 0)
                .toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') 
              } 
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
const styles = {
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyles: {
   fontSize: 16,
   color: '#ffffff',
   padding: 5,
   flex: 1,
   justifyContent: 'flex-start',
  }
};

export default EarningOverallReportRow;
