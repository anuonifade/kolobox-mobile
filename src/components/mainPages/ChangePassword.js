// This file used for confirm password
import React, { Component } from 'react'
import { View, Text, TextInput, Image, TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'
import { Spinner } from '../common'
import { changePassword } from '../../actions/user-actions'

class ConfirmPassword extends Component {
  constructor () {
    super()
    this.state = {
      old_password: '',
      new_cpassword: '',
      new_password: '',
      loading: false
    }
  }
  // API call for update password
  updatePasswordApi () {
    const {new_cpassword, new_password, old_password} = this.state
    const { token, changePassword } = this.props

    if (new_cpassword !== new_password) {
      Alert.alert('Error', 'Passwords do not match', [{ text: 'close',
        onPress: () => {}}])
      return
    }
    this.setState({ loading: true })
    changePassword(token, old_password, new_password)
    .then(({status, data, message}) => {
      if (!status) {
        Alert.alert('Error', message, [{ text: 'close', onPress: () => this.setState({ loading: false }) }])
        return
      }

      Alert.alert('Success', 'Password changed', [{ text: 'close', onPress: () => this.setState({ loading: false }) }])
    })
    .catch(error => {
      Alert.alert('Error', error.message, [{ text: 'close', onPress: () => this.setState({ loading: false, old_password: '', new_password: '', new_cpassword: '' }) }])
    })
  }

  // this method used for render update button
  renderButton () {
    const { loading } = this.state
    if (loading) {
      return <Spinner />
    }

    return (
      <TouchableOpacity onPress={this.updatePasswordApi.bind(this)}>
        <Text style={Styles.ForgotText}>
            Update
          </Text>
      </TouchableOpacity>
    )
  }

  // this method used for render complete UI.
  render () {
    const { cover } = Styles
    return (
      <View style={cover}>
        <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('../img/old_pass.png')} />
            <TextInput
              onChangeText={oldPassword => this.setState({ old_password: oldPassword })}
              underlineColorAndroid='transparent'
              autoCorrect={false}
              secureTextEntry
              placeholder='Old Password'
              placeholderTextColor={'#D3D3D3'}
              style={[Styles.InputStyle, { flex: 1 }]} />
          </View>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('../img/new_pass.png')} />
            <TextInput
              onChangeText={newPassword => this.setState({ new_password: newPassword })}
              underlineColorAndroid='transparent'
              autoCorrect={false}
              secureTextEntry
              placeholder='New Password'
              placeholderTextColor={'#D3D3D3'}
              style={[Styles.InputStyle, { flex: 1 }]} />
          </View>
          <View style={Styles.benefitsContainerStyle}>
            <Image style={{ padding: 0 }} source={require('../img/new_pass.png')} />
            <TextInput
              onChangeText={newCpassword => this.setState({ new_cpassword: newCpassword })}
              underlineColorAndroid='transparent'
              autoCorrect={false}
              secureTextEntry
              placeholder='Confirm Password'
              placeholderTextColor={'#D3D3D3'}
              style={[Styles.InputStyle, { flex: 1 }]} />
          </View>
        </View>
        <View style={Styles.forgotButton}>
          {this.renderButton()}
        </View>
      </View>
    );
  }
}
const Styles = {
  forgotButton: {
    flex: 1,
    alignItems: 'center'

  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  },
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },

  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center'
  },
  InputStyle: {
    flex: 1,
    marginTop: 3,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0

  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 5,
    padding: 0,
    borderBottomWidth: 2,
    borderColor: '#000000'
  }
}

const mapStateToProps = ({ user: {userData: { token }} }) => ({token})

const mapDispatchToProps = ({
  changePassword
})

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmPassword)
