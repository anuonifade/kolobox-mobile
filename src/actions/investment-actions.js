import { AsyncStorage } from 'react-native'
import {
  GET_INVESTMENT_APPROACH
} from '../api/http'
import {
  setInvestmentApproach as setInvestment,
  getInvestmentApproach as getInvestment,
  setShortlistedProducts,
  getShortlistedProducts
} from '../api/local-data'
import {
  asyncRequest,
  asyncReceiveSuccess,
  asyncReceiveError
} from './async-actions'

import {
  GET_INVESTMENTS,
  GET_MY_INVESTMENT_APPROACH,
  SET_MY_INVESTMENT_APPROACH,
  ADD_SHORTLISTED,
  REMOVE_SHORTLISTED,
  GET_SHORTLISTED,
  SET_SHORTLISTED,
  SET_ACTIVE
} from '../constants';



export const getInvestmentApproach = (token, id = '') => dispatch => {
  const action = {type: GET_INVESTMENTS}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // save user data
  return GET_INVESTMENT_APPROACH(token, id)
  .then(responseData => {
    const {status, message} = responseData

    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    dispatch(asyncReceiveSuccess(action))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

/**
 * Get the user investment approach
 */
export const getMyInvestmentApproach = () => dispatch => {
  const action = {type: GET_MY_INVESTMENT_APPROACH}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // get user's investment approach
  return getInvestment()
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

/**
 * Set the user's investment approach
 * @param {Object} investment
 */
export const setMyInvestmentApproach = investment => dispatch => {
  const action = {type: SET_MY_INVESTMENT_APPROACH}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // get user's investment approach
  return setInvestment(investment)
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

/**
 * Get the user investment approach
 */
export const getShortlisted = () => dispatch => {
  const action = {type: GET_SHORTLISTED}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // get shortlisted products
  return getShortlistedProducts()
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

/**
 * Set the user's investment approach
 * @param {Object} investment
 */
export const setShortlisted = products => dispatch => {
  const action = {type: SET_SHORTLISTED}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // save shortlisted products
  return setShortlistedProducts(products)
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

export const addToShortlist = category => dispatch => {
  const action = ADD_SHORTLISTED;
  dispatch({
    type: action,
    product: [...category.Products]
  });
  return;
}

export const removefromShortlist = id => dispatch => {
  const action = REMOVE_SHORTLISTED
  console.log('REMOVE FROM SHORTLIST');
  dispatch({
    type: action,
    id
  });
  return;
}

export const setActiveProduct = id => dispatch => {
  const action = SET_ACTIVE
  dispatch({
    type: action,
    id
  })
}
