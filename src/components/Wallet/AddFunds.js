import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import RangeBar from '../setupAccount1/Slider';
import renderIf from '../common/renderIf';
import BankAccount from '../mainPages/BankAccount';
import NewBankAccount from '../mainPages/NewBankAccount.js';
import DebitCardAdd from '../setupAccount2/debitCardAdd';
import DebitCardDetails from '../mainPages/DebitcardDetails';
import Selector from '../common/selector';


class InvestmentPage extends Component {
  constructor(props) {
  super(props);
    this.state = {
     value: 0,
       status: false
    };
    this.updateSliderValue = this.updateSliderValue.bind(this);
  }
  updateSliderValue(value) {
    this.setState({ value: parseInt(value, 10) });
  }
  toggleStatus() {
    this.setState({
      status: !this.state.status,
    });
  }
  toggleStatus1() {
    this.setState({
        status1: !this.state.status1
    });
  }
  toggleStatus2() {
    this.setState({
      status2: !this.state.status2
    });
  }
  toggleStatus3() {
    this.setState({
        status3: !this.state.status3
    });
  }
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text
          style={{ color: '#2b78e4', fontSize: 18 }}
        >Use the web app for withdrawal of fund</Text>
      </View>
      // <ScrollView>
      //   <View style={{ flex: 1, flexDirection: 'column', margin: 5 }}>
      //     <View style={[styles.benefitsContainerStyle, { marginTop: 10 }]} />
      //     <View style={styles.benefitsContainerStyle}>
      //       <RangeBar
      //         value={0}
      //         minval={0}
      //         maxval={1000000}
      //         updateSliderValue={this.updateSliderValue}
      //       />
      //     </View>
      //     <View style={styles.pickerContainerStyle}>
      //       <Text style={styles.textStyles} >Per</Text>
      //       <Selector />
      //     </View>
      //     <TouchableOpacity onPress={this.toggleStatus.bind(this)}>
      //       <View style={styles.benefitsContainerStyle}>
      //         <Text style={styles.debitCardTextStyle} >Debit Card</Text>
      //         <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
      //       </View>
      //     </TouchableOpacity>
      //     {renderIf(this.state.status)(
      //       <DebitCardDetails />
      //     )}
      //     <View style={{ padding: 0, flexDirection: 'row' }}>
      //       <Text
      //         style={[styles.lineStyle,
      //           {
      //             backgroundColor: '#000000',
      //             marginLeft: 5,
      //             marginRight: 5,
      //             paddingTop: 0
      //           }
      //         ]}
      //       />
      //     </View>
      //     <View>
      //       <TouchableOpacity onPress={this.toggleStatus1.bind(this)}>
      //         <View style={styles.benefitsContainerStyle}>
      //           <Text style={styles.debitCardTextStyle} >Add Debit Card</Text>
      //           <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
      //         </View>
      //       </TouchableOpacity>
      //       {renderIf(this.state.status1)(
      //         <DebitCardAdd />
      //       )}
      //     </View>
      //     <View style={{ padding: 0, flexDirection: 'row' }}>
      //       <Text
      //         style={[styles.lineStyle,
      //           {
      //             backgroundColor: '#000000',
      //             marginLeft: 5,
      //             marginRight: 5,
      //             paddingTop: 0
      //           }
      //         ]}
      //       />
      //     </View>
      //     <View>
      //       <TouchableOpacity onPress={this.toggleStatus2.bind(this)}>
      //         <View style={styles.benefitsContainerStyle}>
      //           <Text style={styles.debitCardTextStyle} >Bank Account</Text>
      //           <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
      //         </View>
      //       </TouchableOpacity>
      //       {renderIf(this.state.status2)(
      //         <BankAccount />
      //       )}
      //     </View>
      //     <View style={{ padding: 0, flexDirection: 'row' }}>
      //       <Text style={[styles.lineStyle,
      //           {
      //             backgroundColor: '#000000',
      //             marginLeft: 5,
      //             marginRight: 5,
      //             paddingTop: 0
      //           }
      //         ]}
      //       />
      //     </View>
      //     <View>
      //       <TouchableOpacity onPress={this.toggleStatus3.bind(this)}>
      //         <View style={styles.benefitsContainerStyle}>
      //           <Text style={styles.debitCardTextStyle} >New Bank Account</Text>
      //           <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
      //         </View>
      //      </TouchableOpacity>
      //       {renderIf(this.state.status3)(
      //         <NewBankAccount />
      //       )}
      //     </View>
      //     <View style={{ padding: 0, flexDirection: 'row' }}>
      //       <Text style={[styles.lineStyle,
      //           {
      //             backgroundColor: '#000000',
      //             marginLeft: 5,
      //             marginRight: 5,
      //             paddingTop: 0
      //           }
      //         ]}
      //       />
      //     </View>
      //   </View>
      // </ScrollView>
    );
  }
}

const styles = {
    HeaderImage: {
    height: 25,
    width: 25,
    padding: 10
  },
    imageView: {

    height: 40,
    flexDirection: 'row',
    margin: 5
  },
  textStyles: {
    fontSize: 16,
    color: '#000000',
    marginTop: 15,
},
  benefitsContainerStyle: {
   flexDirection: 'row',
   marginLeft: 10,
     marginRight: 10,
   padding: 0
 },
 lineStyle: {
   height: 2,
   backgroundColor: '#2b78e4',
   flex: 1,
   marginTop: 10,
     alignItems: 'center',
       justifyContent: 'center',
 },
 arrowImageStyle: {
   height: 25,
   width: 25,
   padding: 5,
   alignSelf: 'flex-end'
 },
 debitCardTextStyle: {
   fontSize: 16,
   color: '#000000',
   flex: 1,
   padding: 5,
   justifyContent: 'center',
   alignItems: 'center'
 },
 pickerContainerStyle: {
   flexDirection: 'row',
   marginLeft: 10,
   marginTop: 30,
     marginRight: 10,
     marginBottom: 30,
   justifyContent: 'center',
     alignItems: 'center'
 }

};
export default InvestmentPage;
