import React, { Component } from 'react';
import { View, TextInput, Image } from 'react-native';
import ModalSelector from 'react-native-modal-selector';

class Selector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textInputValue: ''
    };
  }

  render() {
    let index = 0;
    const data = [
      { key: index++, section: true, label: 'Compounding' },
      { key: index++, label: 'Daily' },
      { key: index++, label: 'Weekly' },
      { key: index++, label: 'Month' },
      { key: index++, label: 'Quarter' },
    ];

    return (
      <View style={{ flexDirection: 'column' }}>

        <ModalSelector
          data={data}
          initValue='Daily'
          supportedOrientations={['portrait']}
          onChange={
            (option) => { this.setState({ textInputValue: option.label }); }
          }
        >
          <View style={{ flexDirection: 'row' }}>
            <TextInput
              style={{
                paddingBottom: 0,
                borderBottomWidth: 1,
                borderColor: '#000',
                width: 110,
                marginLeft: 10,
                height: 30,
                fontSize: 14,
                alignSelf: 'center'
              }}
              editable={false}
              placeholder={this.props.placeholder || 'Compounding'}
              value={this.state.textInputValue}
            />
            <Image style={Styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
          </View>
        </ModalSelector>

      </View>
    );
  }
}

const Styles = {
  arrowImageStyle: {
    height: 25,
    width: 25,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 0
  },
  InputStyle: {
    flex: 1,
    marginTop: 3,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0

  }
};

export default Selector;
