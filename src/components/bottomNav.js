import { NavigationComponent } from 'react-native-material-bottom-navigation';
import { TabNavigator } from 'react-navigation';
import Settings from './mainPages/settings';
import Dashboard from './mainPages/dashboard';
import Earnings from './mainPages/EarningsPage';
import addNew from './mainPages/addnew';
import Profile from './mainPages/MyAccount';

const BottomNav = TabNavigator({
  Dashboard: { screen: Dashboard },
  Earnings: { screen: Earnings },
  New: { screen: addNew },
  Profile: { screen: Profile },
  Settings: { screen: Settings }
}, {
  tabBarComponent: NavigationComponent,
  tabBarPosition: 'bottom',
  tabBarOptions: {
    bottomNavigationOptions: {
      labelColor: 'white',
      rippleColor: 'white',
      tabs: {
        Dashboard: {
          barBackgroundColor: '#3d93cb'
        },
        Earnings: {
          barBackgroundColor: '#3d93cb'
        },
        New: {
          barBackgroundColor: '#3d93cb'
        },
        Profile: {
          barBackgroundColor: '#3d93cb'
        },
        Settings: {
          barBackgroundColor: '#3d93cb'
          // labelColor: '#000',
          // activeLabelColor: '#212121',
          // activeIcon: <Icon size={24} color="#212121" name="settings" />
        }
      }
    }
  }
});
export default BottomNav;
