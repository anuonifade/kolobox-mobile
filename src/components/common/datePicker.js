/* date of birth picker */
import React, { Component } from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { DatePickerDialog } from 'react-native-datepicker-dialog'
import moment from 'moment'

export default class DatePicker extends Component {

  constructor(props) {
    super(props)
    this.state = {
      dobText: moment(this.props.date || new Date()).format('YYYY-MM-DD'),
      dobDate: this.props.date || new Date(),
      journeyText: '',
      journeyDate: null,
    }
  }


  onDOBPress = () => {
    let dobDate = this.state.dobDate

    if (!dobDate || dobDate == null) {
      dobDate = new Date()
      this.setState({
        dobDate
      })
    }

    //To open the dialog
    this.refs.dobDialog.open({
      date: dobDate,
      maxDate: new Date() //To restirct future date
    })
  }

  /**
   * Call back for dob date picked event
   *
   */
  onDOBDatePicked = (date) => {
    this.setState({
      dobDate: date,
      dobText: moment(date).format('YYYY-MM-DD')
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.dobText !== this.state.dobText)
      this.props.handleToUpdate(this.state.dobText)
  }
  render() {
    console.log(this.props)
    console.log(this.state)
    return (
      <View style={styles.containerStyle}>
        <View style={styles.labelStyle}>
          <Icon name={this.props.label} size={25} color='grey' style={{ padding: 2 }} />
        </View>
        <TouchableOpacity
          onPress={this.onDOBPress.bind(this)}
          style={{ flex: 1, justifyContent: 'center', flexWrap: 'wrap' }}
        >
          <Text style={[styles.InputStyle, this.state.dobText && { color: '#000' }]} onChangeText={this.props.onChangeText}>
            {this.state.dobText || 'Date of Birth'}
          </Text>
        </TouchableOpacity>

        <DatePickerDialog
          ref="dobDialog"
          onDatePicked={this.onDOBDatePicked.bind(this)}
        />

      </View>
    )
  }
}

const styles = {
  InputStyle: {
    flex: 1,
    height: 30,
    marginTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0,
    color: '#D3D3D3',
  },
  labelStyle: {

    alignItems: 'center',
    justifyContent: 'center',
    width: 35
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    height: 45,
    borderBottomWidth: 2,
    borderColor: '#D3D3D3'
  },
  datePickerBox: {
    marginTop: 9,
    borderColor: '#ABABAB',
    borderWidth: 0.5,
    padding: 0,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    height: 38,
    justifyContent: 'center'
  },
  datePickerText: {
    fontSize: 14,
    marginLeft: 5,
    borderWidth: 0,
  },
};
