import React, { Component } from 'react';
import { Text, View, Image, Picker } from 'react-native';
import RangeBar from './Slider';
import { Input } from '../common';
import Selector from '../common/selector';

class AccountDetailPage extends Component {
  constructor(props) {
     super(props);
     this.state = { value: 40,
    };
}
onSelect(index, value) {
  this.setState({
  text: value
  });
}
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <Text style={[Styles.textStyles, { margin: 10 }]} >Amount(in ₦)</Text>
        <View style={Styles.benefitsContainerStyle}>
          <RangeBar
          value={this.state.value}
          maximumValue={1000000}
          onValueChange={value => this.setState({ value })}
          />
        </View>
        <View style={Styles.pickerContainerStyle}>
          <Text style={Styles.textStyles} >Per</Text>
          <Selector />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <Text style={Styles.lineStyle} />
          <Text style={Styles.benefitTextStyle} >Add money using</Text>
          <Text style={Styles.lineStyle} />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <Text style={Styles.debitCardTextStyle} >Debit Card</Text>
          <Image style={Styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <Text style={Styles.lineStyle} />
          <Text style={Styles.benefitTextStyle} >Or</Text>
          <Text style={Styles.lineStyle} />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <Text style={Styles.debitCardTextStyle} >Bank Account</Text>
          <Image style={Styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <Text style={Styles.lineStyle} />
        </View>
      </View>

    );
  }

}

const Styles = {

  textStyles: {
    fontSize: 16,
    color: '#000000',
    marginTop: 15,
},
 benefitsContainerStyle: {
  flexDirection: 'row',
  marginLeft: 10,
    marginRight: 10,
  padding: 0
},
lineStyle: {
  height: 2,
  backgroundColor: '#2b78e4',
  flex: 1,
  marginTop: 10,
    alignItems: 'center',
      justifyContent: 'center',
},
benefitTextStyle: {
  fontSize: 16,
  color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
  marginBottom: 10,
  alignItems: 'center',
  justifyContent: 'center',
},
bulletStyle: {
  fontSize: 27,
  color: '#000000',

},
bulletTextStyle: {
  fontSize: 16,
  color: '#000000',
  margin: 5,
},
container: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  selectedPriceTextStyle: {
  borderBottomColor: '#000000',
  borderBottomWidth: 1,
   paddingBottom: 5,
  flex: 1,
  alignItems: 'center'
},
pickerContainerStyle: {
  flexDirection: 'row',
  marginLeft: 10,
  marginTop: 30,
    marginRight: 10,
    marginBottom: 30,
  justifyContent: 'center',
    alignItems: 'center'
},
containerRAdio: {
       flexDirection: 'row',
   },
   text: {
       padding: 10,
       fontSize: 14,
   },
   graphTextStyle: {
     fontSize: 16,
     color: '#000000',
     margin: 10,
     padding: 10
   },
   arrowImageStyle: {
     height: 25,
     width: 25,
     padding: 5,
     justifyContent: 'flex-end'
   },
   debitCardTextStyle: {
     fontSize: 16,
     color: '#000000',
     flex: 1
   }

};

export default AccountDetailPage;
