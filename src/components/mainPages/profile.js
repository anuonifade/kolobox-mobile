import React from 'react';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AccountHeader } from '../common';

class Profile extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Account',
    tabBarIcon: () => (<Icon size={24} color="white" name="person" />)
  }

  render() {
    return (
      <View>
        <AccountHeader showHelp showWallet headerTxt={'Kolonaija'} />
        <View style={{ flex: 1 }}>
        <Text style={{ alignItems: 'center', justifyContent: 'center', fontSize: 20 }}>Welcome to Profile Page!</Text>
        </View>
        </View>
     );
   }
}

export default Profile;
