import React from 'react'
import { View } from 'react-native'

const Card = (props) => (
  <View style={styles.containerStyle}>
    {props.children}
  </View>
)

const styles = {
  containerStyle: {
    borderWidth: 1,
    borderRadius: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#fff',
    shadowOpacity: 0.1,
    shadowOffset: { width: 0, height: 1 },
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  }
}

export { Card }
