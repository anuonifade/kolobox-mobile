// This file used for Account info
import React from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView } from 'react-native'
import BasicInfo from './BasicInfo.js'
import UpdatePIN from './UpdatePin.js'
import renderIf from '../common/renderIf'

class MyAccountMyInfo extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      value: 40,
      status: false,
      status1: false
    }
  }

  toggleStatus () {
    this.setState({
      status: !this.state.status
    })
  }
  toggleStatus1 () {
    this.setState({
      status1: !this.state.status1
    })
  }
  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>

        <ScrollView >
          <TouchableOpacity onPress={this.toggleStatus.bind(this)}>
            <View style={styles.benefitsContainerStyle}>
              <Text style={styles.debitCardTextStyle} >Basic Info</Text>
              <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
            </View>
          </TouchableOpacity>
          {renderIf(this.state.status)(
            <BasicInfo />
           )}
          <View style={{ padding: 0, flexDirection: 'row' }}>
            <Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, paddingTop: 0 }]} />
          </View>
          <View>

            <TouchableOpacity onPress={this.toggleStatus1.bind(this)}>
              <View style={styles.benefitsContainerStyle}>
                <Text style={styles.debitCardTextStyle} >PIN</Text>
                <Image style={styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
              </View>
            </TouchableOpacity>
            {renderIf(this.state.status1)(
              <UpdatePIN />
        )}
          </View>
          <View style={{ padding: 0, flexDirection: 'row' }}>
            <Text style={[styles.lineStyle, { backgroundColor: '#000000', marginLeft: 5, marginRight: 5, paddingTop: 0 }]} />
          </View>

        </ScrollView>
      </View>

    )
  }
}

const styles = {

  blacklineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
  graphTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
    padding: 10
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 10,
    padding: 0
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonStyle: {
    height: 30,
    width: 120,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb'
  },
  blackTextStyles: {
    fontSize: 16,
    color: '#3d93cb',
    borderBottomWidth: 2,
    borderColor: '#3d93cb'
  }

}
export default MyAccountMyInfo
