import {
  ADD_SHORTLISTED,
  REMOVE_SHORTLISTED,
  SET_ACTIVE
} from '../constants';


const _addToList = (state, action) => {
  const { product } = action
  return {
    ...state,
    shortlisted: { ...state.shortlisted, product}
  }
}

const _removeFromList = (state, action) => {
  const { id } = action
  const newList = { ...state.shortlisted }
  delete newList[id]
  return {
    ...state,
    shortlisted: newList
  }
}

const _setActive = (state, action) => {
  const { id } = action
  return {
    ...state,
    id
  }
}
initialState = {
  shortlisted: {
    product: []
  }
}
export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_SHORTLISTED: return _addToList(state, action)
    case REMOVE_SHORTLISTED: return _removeFromList(state, action)
    case SET_ACTIVE: return _setActive(state, action)
    default:
      return state
  }
}
