import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import SavingsBarChart from './graph';
import GraphDetails from './GraphDetails';
import { getUserDashboard } from '../../../actions/user-actions';

class SavingGraph extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: '',
      userData: '',
      totalSavings: 0.000,
      averageSavings: 0.000,
      data: [],
      keys: [],
      investment: [],
      chartStart: -5,
      chartEnd: undefined,
      dashboard: {},
    };
    this.renderChartData = this.renderChartData.bind(this);
    this.loadDashboard = this.loadDashboard.bind(this);
    this.plotChart = this.plotChart.bind(this);
    this.startOfChart = this.startOfChart.bind(this);
    this.endOfChart = this.endOfChart.bind(this);
  }

  componentWillMount() {
    AsyncStorage.getItem('userData')
      .then((result) => {
        const userData = JSON.parse(result);
        const { getUserDashboard } = this.props;
        this.setState({ loading: true });
        getUserDashboard(userData.token)
          .then((dashboard) => {
            this.setState({ dashboard: dashboard.data });
            this.loadDashboard(dashboard.data);
          }).catch((error) => {
            this.setState({
              loading: false,
              error: `Click to Reload ${error}`
            });
          });
      });
  }

  loadDashboard(dashboard) {
    this.setState({
      loading: false,
      totalSavings: parseFloat(dashboard.savings.data.total_savings || 0).toFixed(2),
      averageSavings: parseFloat(dashboard.savings.data.average_savings || 0).toFixed(2),
      data: this.renderChartData(dashboard),
    });
  }

  plotChart() {
    if (this.state.error && this.state.error !== '') {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', top: 10 }}>
          <Text style={{ fontWeight: 'bold', color: '#fff' }} >
            { this.state.error }
          </Text>
        </View>
      );
    }
    return (
      <SavingsBarChart chart={this.state.data} keys={this.state.keys} />
    );
  }

  startOfChart() {
    this.setState({
      loading: true,
      chartStart: 0,
      chartEnd: 5,
    });
    this.loadDashboard(this.state.dashboard);
    this.plotChart();
  }

  endOfChart() {
    this.setState({
      loading: true,
      chartStart: -5,
      chartEnd: undefined,
    });
    this.loadDashboard(this.state.dashboard);
    this.plotChart();
  }

  renderChartData(dashboard) {
    const responses = [];
    const uniqueKeys = [];
    if (dashboard.savings.chart && dashboard.savings.chart.length > 0) {
      dashboard
        .savings
          .chart
            .forEach((saving) => {
              if (responses.find((x) => x.date.getTime() === (new Date(saving.date)).getTime())) {
                responses
                  .find(x => x.date.getTime() === (new Date(saving.date)).getTime())[saving.name]
                    = parseFloat(saving.amount || 0).toFixed(2);
              } else {
                const obj = {};
                obj.date = new Date(saving.date);
                obj[saving.name] = parseFloat(saving.amount || 0).toFixed(2);
                responses.push(obj);
              }
              uniqueKeys.indexOf(saving.name) === -1
              ? uniqueKeys.push(saving.name)
              : console.log('Key already exist');
            });
    }
    this.setState({
      keys: uniqueKeys,
      investment: uniqueKeys,
      loading: false,
    });

    responses.sort((a, b) => {
      const keyA = new Date(a.date);
      const keyB = new Date(b.date);
      // Compare the 2 dates
      if (keyA < keyB) return -1;
      if (keyA > keyB) return 1;
      return 0;
    });

    return responses.slice(this.state.chartStart, this.state.chartEnd);
  }


  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <View>
          <Text
            style={{
              justifyContent: 'flex-end',
              alignSelf: 'flex-end',
              margin: 3
            }}
          >
            *Savings Projected
          </Text>
        </View>
        <ScrollView style={{ flex: 1, top: 20 }}>
        {
          this.state.loading
          ? <ActivityIndicator size="large" color="#1034A6" />
          : this.plotChart()
        }
        {
          this.state.error && this.state.error !== ''
          ? <View />
          : <GraphDetails
              investment={this.state.investment}
              labelTotal="Total Savings"
              labelAverage="Average Savings"
              total={this.state.totalSavings}
              average={this.state.averageSavings}
          />
        }
        </ScrollView>
      </View>

    );
  }
}
// Styles used for View adjustment

const Styles = {
  buttonStyle: {
    height: 40,
    width: 170,
    marginBottom: 5,
    marginTop: 10,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb',
    paddingTop: 10,
    paddingBottom: 10,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
  }
};

const mapStateToProps = (state) => {
  const { userData, dashboard } = state.user;
  return {
    userData,
    dashboard
  };
};

const mapDispatchToProps = ({
  getUserDashboard
});


export default connect(mapStateToProps, mapDispatchToProps)(SavingGraph);
