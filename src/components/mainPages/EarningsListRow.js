//This file used for Earning list row
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import Moment from 'moment'
import { Actions } from 'react-native-router-flux';

class EarningListRow extends Component {
   //used for render views
  render() {
    const routeTo = () => { Actions.EarningNameOfDeposit(); };
    Moment.locale('en');
    const startDate = new Date(this.props.data.start_date);
    return (
      <View style={{ flex: 1 }} >
        <ScrollView style={{ flex: 1 }} >
          <View style={styles.containerStyle}>
            <View style={styles.childContainerStyle}>
             <TouchableOpacity
               onPress={routeTo.bind(this)}
               style={{ flex: 1, justifyContent: 'flex-start' }}
             >
              <Text style={styles.textStyles}>{ this.props.data.name }</Text>
             </TouchableOpacity>
            </View>

            <View style={styles.accountContainerStyle}>
              <View style={[styles.leftViewStyle, { marginLeft: 40 }]}>
                <Text style={styles.blackTextStyles}>INTEREST</Text>
                <Text style={styles.blackTextStyles}>
                  {parseFloat(this.props.data.interest_rate || 0).toFixed(2)}%
                </Text>
              </View>
              <View style={[styles.rightViewStyle, { marginRight: 40 }]}>
                <Text style={styles.blackTextStyles}>START DATE</Text>
                <Text style={styles.blackTextStyles}>
                  { Moment(startDate).format('Do MMM YYYY')}
                </Text>
              </View>
            </View>

            <View style={styles.accountContainerStyle}>
              <View style={[styles.leftViewStyle, { marginLeft: 40 }]}>
                <Text style={styles.blackTextStyles}>INVESTMENT</Text>
                <Text style={styles.blackTextStyles}>₦{ parseFloat(this.props.data.amount || 0)
                    .toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                </Text>
              </View>
              <View style={[styles.rightViewStyle, { marginRight: 40 }]}>
                <Text style={styles.blackTextStyles}>AVG INTEREST</Text>
                <Text style={styles.blackTextStyles}>₦{ parseFloat(this.props.data.interest || 0)
                    .toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                  }
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>

    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    borderColor: '#4991bf',
    borderWidth: 2,
    margin: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#4991bf',
    padding: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
   textStyles: {
    fontSize: 16,
    color: '#ffffff',
    padding: 5,
    flex: 1,
    justifyContent: 'flex-start',
  },
  blackTextStyles: {
   fontSize: 16,
   color: '#000000',
   padding: 5,
 },
  interestContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#4991bf',
      padding: 5
  },
  accountContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 5
  },
  leftViewStyle: {
      flex: 1,
        alignItems: 'flex-start',
      justifyContent: 'flex-start'
  },
  centerViewStyle: {
      flex: 1,
        alignItems: 'center',
      justifyContent: 'center'
  },
  arrowImageView: {

      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      flexDirection: 'row'
  }
};

export default EarningListRow;
