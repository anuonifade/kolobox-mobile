// {This file is used to enter pin to navigate into next screen.}
import React, { Component } from 'react'
import { Text, View, TextInput, Keyboard, AsyncStorage, Alert, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { AccountHeader, FullWidthButton, Spinner } from './common'

class Validatecode extends Component {
  constructor (props) {
    super(props)
    this.data = null
    this.state = { text1: '', text2: '', text3: '', text4: '', verifying: false, getting: false, isReady: false }
  }

  verifyningPin () {
    console.log('inapi')
    if (this.state.text1 === '' || this.state.text2 === '' ||
      this.state.text3 === '' || this.state.text4 === '') {
      Alert.alert(
        'Error!',
        'Please Fill all the fields',
        [
          { text: 'OK' }
        ],
        { cancelable: false }
      )
      return
    }
    AsyncStorage.getItem('forgotpasstoken', (err, result) => {
      this.data = JSON.parse(result)
      this.setState({ verifying: true })
      console.log(this.data.token)
      console.log('hello')
      fetch(GLOBALS.BASE_URL + 'auth/user/password_reset/validate', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.data.token
        },
        body: JSON.stringify({
          code: (this.state.text1 + this.state.text2 + this.state.text3 + this.state.text4).toString()
        })
      }).then((response) => response.json()
        .then(responseJson => {
          if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
            AsyncStorage.removeItem('forgotpasstoken', (error) => {
              if (error !== null) {
                console.log('unable to log out')
              }
              Actions.loginPages()
            })
          } else {
            this.setState({ verifying: false })
            if (responseJson.status === true) {
              AsyncStorage.setItem('userData', JSON.stringify(responseJson.data))
              console.log(responseJson)
              Alert.alert(
                'Code Verified',
                'Press ok to create new Password.',
                [
                  { text: 'OK', onPress: () => Actions.createnewpassword() } // main
                ],
                { cancelable: false }
              )
            } else {
              Alert.alert(
                'Error!',
                responseJson.message,
                [
                  { text: 'OK' }
                ],
                { cancelable: false }
              )
            }
          }
        })).catch((error) => {
          console.error(error)
        })
    }) // async getdata block ends...
  }
  resendCodeApi () {
    AsyncStorage.getItem('forgotpasstoken', (err, result) => {
      this.data = JSON.parse(result)
      this.setState({ getting: true })
      console.log(this.data.token)
      console.log('hello')
      fetch(GLOBALS.BASE_URL + 'auth/user/resend/confirmation_code', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.data.token
        },
        body: JSON.stringify({
          emailPhone: this.data.email
        })
      }).then((response) => response.json()
        .then(responseJson => {
          console.log('hello')
          if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
            AsyncStorage.removeItem('signupData', (error) => {
              if (error !== null) {
                console.log('unable to log out')
              }
              Actions.loginPages()
            })
          } else {
            this.setState({ getting: false })
            console.log('in the code')
            if (responseJson.status === true) {
              Alert.alert(
                'Success',
                'New code sent to your email id.',
                [
                  { text: 'OK' } // main
                ],
                { cancelable: false }
              )
            } else {
              Alert.alert(
                'Error!',
                responseJson.message,
                [
                  { text: 'OK' }
                ],
                { cancelable: false }
              )
            }
          }
        })).catch((error) => {
          console.error(error)
        })
    }) // async getdata block ends...
  }
  renderCode () {
    if (!this.state.getting) {
      return (
        <TouchableOpacity onPress={this.resendCodeApi.bind(this)}>
          <Text style={Styles.ForgotText}>
            Resend Code
          </Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <Spinner />
      )
    }
  }
  renderButton () {
    if (!this.state.verifying) {
      return (
        <FullWidthButton onPress={this.verifyningPin.bind(this)} />
      )
    } else {
      return (
        <Spinner />
      )
    }
  }
  // This render method used for rendering UI.
  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <AccountHeader headerTxt='Kolobox' showHelp />
        <View style={Styles.componentStyle}>
          <Text style={Styles.textStyles}>
            Welcome '
            {'UserName'}', Please Enter code
          </Text>
          <View style={Styles.textInputContainerStyle}>
            <TextInput
              autoFocus
              style={Styles.textInputStyle}
              onChangeText={(text1) => {
                this.setState({ text1 })
                if (text1 && text1.length === 1) {
                  this.refs.SecondInput.focus()
                }
              }}
              value={this.state.text1}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.SecondInput.focus()
              }} />
            <TextInput
              ref='SecondInput'
              style={Styles.textInputStyle}
              onChangeText={(text2) => {
                this.setState({ text2 })
                if (text2 && text2.length === 1) {
                  this.refs.ThirdInput.focus()
                }
              }}
              value={this.state.text2}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.ThirdInput.focus()
              }} />
            <TextInput
              ref='ThirdInput'
              style={Styles.textInputStyle}
              onChangeText={(text3) => {
                this.setState({ text3 })
                if (text3 && text3.length === 1) {
                  this.refs.FourthInput.focus()
                }
              }}
              value={this.state.text3}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.FourthInput.focus()
              }} />
            <TextInput
              ref='FourthInput'
              style={Styles.textInputStyle}
              onChangeText={text4 => {
                this.setState({ text4 })

                if (text4 && text4.length === 1) {
                  Keyboard.dismiss()
                }
              }}
              value={this.state.text4}
              keyboardType='numeric'
              maxLength={1} />
          </View>
          <View style={{ alignItems: 'center' }} />
          {this.renderButton()}
        </View>
      </View>

    )
  }
}

// Style components

const Styles = {
  textStyles: {
    fontSize: 20,
    color: '#000000',
    marginTop: 20
  },
  componentStyle: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',

    flexDirection: 'column',
    padding: 1,
    flex: 1

  },
  textInputContainerStyle: {
    flexDirection: 'row',
    marginTop: 10

  },
  textInputStyle: {
    height: 100,
    width: 80,
    borderColor: 'gray',
    borderWidth: 2,
    margin: 5,
    flex: 2,
    fontSize: 30,
    alignSelf: 'center',
  },
  buttonStyle: {
    height: 40,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#4991bf',
    borderColor: '#4991bf'
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',

    textDecorationLine: 'underline'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#ffffff',
    fontWeight: '600',
    paddingTop: 10

  }
}

export default Validatecode
