import React from 'react';
import { Text, Image, View } from 'react-native';

const AccountHeader = ({ showHelp, headerTxt, showWallet, showSkip, onPressSkip, onPressWallet, onPressHelp }) => {
  const { textStyles, HeaderImage, imageView, componentStyle } = Styles
  return (
    <View style={{ flexDirection: 'column', alignItems: 'center' }}>
      <View style={componentStyle}>
        <View style={imageView} >
          <Image style={HeaderImage} source={require('../img/kolo_logo.png')} />
          <Text style={textStyles} >{headerTxt}</Text>
        </View>
      </View>
    </View>
  )
}

// Style components

const Styles = {

  HeaderImage: {
    height: 25,
    width: 25,
    marginTop: 8
  },
  walletHeaderImage: {
    height: 20,
    width: 20
  },
  textView: {

    alignItems: 'flex-start',
    flex: 1
  },
  imageView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row'

  },
  textStyles: {
    paddingLeft: 5,
    fontSize: 20,
    color: '#ffffff',
    marginTop: 8
  },
  withdrawaltextStyles: {
    paddingLeft: 5,
    fontSize: 15,
    color: '#ffffff',
    marginTop: 5
  },

  componentStyle: {
    backgroundColor: '#3d93cb',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'relative',
    padding: 1,
    height: 70

  },
  buttonStyle: {
    height: 40,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb'
  }

}

// make component available to other parts of App

export { AccountHeader }
