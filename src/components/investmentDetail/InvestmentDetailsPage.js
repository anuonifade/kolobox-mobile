// {this the main InvestmentDetail page. All the screen of investment rendered through it.  }
import React, { Component } from 'react';
import { View, Text } from 'react-native';
// import CheckBox from 'react-native-check-box';
import { AccountHeader } from '../common';
import InvestmentDetailsTabs from './InvestmentDetailsTabs';

class InvestmentDetailsPage extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AccountHeader showSkip headerTxt={'Kolobox'} />
        <View style={Styles.imageView}>
          <Text style={Styles.textStyles}>
            Investment Name: {this.props.product.name}
          </Text>

        </View>
        <InvestmentDetailsTabs product={this.props.product} />
      </View>
    );
  }
}
// Styles used for View adjustment
const Styles = {
  HeaderImage: {
    height: 25,
    width: 25,
    padding: 10
  },
  imageView: {
    height: 40,
    flexDirection: 'row',
    margin: 5
  },
  textStyles: {
    flex: 1,
    fontSize: 14,
    color: '#000000',
    padding: 5,
    marginTop: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    marginTop: 5
  }
};

export default InvestmentDetailsPage;
