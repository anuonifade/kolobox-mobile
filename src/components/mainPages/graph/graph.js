import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { StackedBarChart, YAxis, XAxis } from 'react-native-svg-charts';
import Moment from 'moment';

export default class SavingsBarChart extends PureComponent {

  render() {
    Moment.locale('en');

    const date = [];
    this.props.chart.map((values) => date.push(values.date));
    const colors = [
      '#A8C3C8',
      '#C1BADB',
      '#CEA8BC',
      '#A8C3C8',
      '#C1BADB'
    ];

    return (
      <View style={{ flex: 1, flexDirection: 'column', height: 300 }}>
        <View style={{ flex: 1, flexDirection: 'row', height: 290 }}>
          <StackedBarChart
            style={{ flex: 1, marginLeft: 30, marginRight: 10 }}
            keys={this.props.keys}
            colors={colors}
            data={this.props.chart}
            contentInset={{ top: 20, bottom: 30, left: 10, right: 10 }}
          />
          <YAxis
            style={{ position: 'absolute', top: 0, bottom: 0 }}
            data={StackedBarChart.extractDataPoints(this.props.chart, this.props.keys)}
            contentInset={{ top: 20, bottom: 30 }}
            svg={{
                fontSize: 8,
                fill: 'white',
                stroke: 'black',
                strokeWidth: 0.2,
                alignmentBaseline: 'baseline',
                baselineShift: '3',
            }}
          />
        </View>
        <View>
          <XAxis
            style={{ height: 10, bottom: 10, flexDirection: 'row' }}
            data={date}
            contentInset={{ left: 20, right: 10 }}
            xAccessor={(value) => value}
            formatLabel={(value) => Moment(value).format('Do MMM YYYY')}
            svg={{ fontSize: 8 }}
          />
        </View>
      </View>
    );
  }
}
