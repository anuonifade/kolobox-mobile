// {This file is used to update user pin.}
import React, { Component } from 'react'
import { View, Text, TextInput, TouchableOpacity, Keyboard, Alert } from 'react-native'
import { connect } from 'react-redux'
import { Spinner } from '../common'

import { updateUserPin } from '../../actions/user-actions'

export class UpdatePIN extends Component {
  constructor (props) {
    super(props)
    this.state = {
      text1: '',
      text2: '',
      text3: '',
      text4: '',
      text5: '',
      text6: '',
      text7: '',
      text8: '',
      loading: false
    }
  }

  // this method contain Api to update user pin.
  updatePin () {
    if (this.state.text1 === '' || this.state.text2 === '' || this.state.text3 === '' || this.state.text4 === '' ||
      this.state.text5 === '' || this.state.text6 === '' || this.state.text7 === '' || this.state.text8 === '') {
      Alert.alert('Error!', 'Please Fill all the fields', [{ text: 'OK', onPress: () => {} }], { cancelable: false })
      return
    }

    const { updateUserPin, token } = this.props

    const oldPin = this.state.text1 + this.state.text2 + this.state.text3 + this.state.text4
    const newPin = this.state.text5 + this.state.text6 + this.state.text7 + this.state.text8
    this.setState({ loading: true })

    updateUserPin(token, oldPin, newPin)
    .then(({status, message, data}) => {
      if (!status) {
        Alert.alert(
          'Error',
          message,
          [
            { text: 'OK', onPress: () => this.setState({ loading: false }) }
          ],
          { cancelable: false }
        )
        return
      }

      Alert.alert(
        'Success!',
        'Pin updated',
        [
          { text: 'OK',
            onPress: () => {
              this.setState({ loading: false, text1: '', text2: '', text3: '', text4: '', text5: '', text6: '', text7: '', text8: '' })
            }}
        ],
        { cancelable: false }
      )
    })
    .catch(error => {
      Alert.alert(
        'Error',
        error.message,
        [
          { text: 'OK', onPress: () => this.setState({ loading: false }) }
        ],
        { cancelable: false }
      )
      throw error
    })
  }

  // This method is used for to show activity indication during pin update.
  renderButton () {
    const { loading } = this.state
    if (loading) {
      return <Spinner />
    }

    return (
      <TouchableOpacity onPress={this.updatePin.bind(this)} style={{ alignItems: 'center' }}>
        <Text style={Styles.ForgotText}>
          Change
        </Text>
      </TouchableOpacity>
    )
  }
  // This method is used to show all views.
  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
          <Text style={Styles.textStyles}>
            Enter Old Pin
          </Text>
          <View style={Styles.textInputContainerStyle}>
            <TextInput
              autoFocus
              style={Styles.textInputStyle}
              onChangeText={(text1) => {
                this.setState({ text1 })
                if (text1 && text1.length === 1) {
                  this.refs.SecondInput.focus()
                }
              }}
              value={this.state.text1}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.SecondInput.focus()
              }} />
            <TextInput
              ref='SecondInput'
              style={Styles.textInputStyle}
              onChangeText={(text2) => {
                this.setState({ text2 })
                if (text2 && text2.length === 1) {
                  this.refs.ThirdInput.focus()
                }
              }}
              value={this.state.text2}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.ThirdInput.focus()
              }} />
            <TextInput
              ref='ThirdInput'
              style={Styles.textInputStyle}
              onChangeText={(text3) => {
                this.setState({ text3 })
                if (text3 && text3.length === 1) {
                  this.refs.FourthInput.focus()
                }
              }}
              value={this.state.text3}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.FourthInput.focus()
              }} />
            <TextInput
              ref='FourthInput'
              style={Styles.textInputStyle}
              onChangeText={(text4) => {
                this.setState({ text4 })
                if (text4 && text4.length === 1) {
                  Keyboard.dismiss()
                }
              }}
              value={this.state.text4}
              keyboardType='numeric'
              maxLength={1} />
          </View>
        </View>
        <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
          <Text style={Styles.textStyles}>
            Enter New Pin
          </Text>
          <View style={Styles.textInputContainerStyle}>
            <TextInput
              autoFocus
              style={Styles.textInputStyle}
              onChangeText={(text5) => {
                this.setState({ text5 })
                if (text5 && text5.length === 1) {
                  this.refs.SixthInput.focus()
                }
              }}
              value={this.state.text5}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.SixthInput.focus()
              }} />
            <TextInput
              ref='SixthInput'
              style={Styles.textInputStyle}
              onChangeText={(text6) => {
                this.setState({ text6 })
                if (text6 && text6.length === 1) {
                  this.refs.SeventhInput.focus()
                }
              }}
              value={this.state.text6}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.SeventhInput.focus()
              }} />
            <TextInput
              ref='SeventhInput'
              style={Styles.textInputStyle}
              onChangeText={(text7) => {
                this.setState({ text7 })
                if (text7 && text7.length === 1) {
                  this.refs.EighthInput.focus()
                }
              }}
              value={this.state.text7}
              keyboardType='numeric'
              maxLength={1}
              onSubmitEditing={(event) => {
                this.refs.EighthInput.focus()
              }} />
            <TextInput
              ref='EighthInput'
              style={Styles.textInputStyle}
              onChangeText={(text8) => {
                this.setState({ text8 })
                if (text8 && text8.length === 1) {
                  Keyboard.dismiss()
                }
              }}
              value={this.state.text8}
              keyboardType='numeric'
              maxLength={1} />
          </View>
        </View>
        {this.renderButton()}
      </View>
    )
  }
}
const Styles = {
  textInputContainerStyle: {
    flexDirection: 'row',
    marginTop: 10
  },
  textInputStyle: {
    height: 100,
    width: 80,
    borderColor: 'gray',
    borderWidth: 2,
    margin: 5,
    flex: 2,
    fontSize: 30,
    alignSelf: 'center',
  },
  blackTextStyles: {
    fontSize: 16,
    color: '#3d93cb',
    borderBottomWidth: 2,
    borderColor: '#3d93cb'
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  },
  textStyles: {
    fontSize: 16,
    color: '#000000',
    padding: 5,
    justifyContent: 'flex-start',
  }
};

const mapStateToProps = ({ user: {userData: { token }} }) => ({token});

const mapDispatchToProps = ({
  updateUserPin
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePIN);
