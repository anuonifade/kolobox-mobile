import { combineReducers } from 'redux'

import async from './async-reducer'
import user from './user-reducer'
import investment from './investment-reducer'
import toast from './toast-reducer'

const rootReducer = combineReducers({
  async,
  user,
  investment,
  toast
})

export default rootReducer
