import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {
  createNavigator,
  createNavigationContainer,
  TabRouter,
  addNavigationHelpers
} from 'react-navigation';
import RegisterForm from './registerform';
import LoginForm from './loginform';

// const used for navigate tab button
const CustomTabBar = ({ navigation }) => {
  const { routes } = navigation.state;
  return (
    <View style={styles.tabContainer}>
      {routes.map(route => (
        <TouchableOpacity
          onPress={() => navigation.navigate(route.routeName)}
          style={styles.tab}
          key={route.routeName}
        >
          <Text>{route.routeName}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

// used for create custom tabs
const CustomTabView = ({ router, navigation }) => {
  const { routes, index } = navigation.state;
  const ActiveScreen = router.getComponentForState(navigation.state);
  const routeNav = addNavigationHelpers({
    ...navigation,
    state: routes[index]
  });

  return (
    <View style={styles.container}>
      <CustomTabBar navigation={navigation} />
      <ActiveScreen navigation={routeNav} />
    </View>
  );
};

const CustomTabRouter = TabRouter(
  {
    'Sign Up': {
      screen: RegisterForm,
      path: ''
    },
    'Sign In': {
      screen: LoginForm,
      path: ''
    }
  },
  {
    initialRouteName: 'Sign Up'
  }
);

const CustomTabs = createNavigationContainer(
  createNavigator(CustomTabRouter)(CustomTabView)
);

const styles = StyleSheet.create({
  container: {
    flex: 1
    // marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  tabContainer: {
    flexDirection: 'row',
    height: 48
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 4,
    borderWidth: 1,
    borderColor: '#ddd',
    backgroundColor: 'white'
  }
});

export default CustomTabs;
