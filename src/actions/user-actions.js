import { AsyncStorage } from 'react-native';
import { asyncRequest, asyncReceiveSuccess, asyncReceiveError } from './async-actions'
import {
  saveUserData as setUser,
  getUserData as getUser,
  setFirstTimeUser as setFirstTime,
  getFirstTimeUser as getFirstTime
} from '../api/local-data'
import {
  SIGNUP,
  LOGIN,
  VERIFY_PIN,
  RESEND_PIN,
  SET_USER_PIN as SET_PIN,
  GET_USER_PROFILE as GET_PROFILE,
  UPDATE_PASSWORD,
  UPDATE_USER_PROFILE,
  UPDATE_PIN,
  GET_DASHBOARD,
  GET_EARNINGS,
  GET_OVERALL_EARNINGS
} from '../api/http'
import * as constants from '../constants'

/**
 * saves user data locally
 * @param {string} userData
 */
export const setUserData = userData => dispatch => {
  const action = {type: constants.SET_USER_DATA}
  const actionSetToken = {type: constants.SET_TOKEN}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // save user data
  return setUser(userData)
  .then(() => {
    const { token } = userData
    dispatch(asyncReceiveSuccess(action))
    dispatch({...action, userData})
    dispatch({...actionSetToken, token})

    return userData
  })
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

/**
 * get user's data if available
 */
export const getUserData = () => dispatch => {
  const action = {type: constants.GET_USER_DATA}

  const actionSetUser = {type: constants.SET_USER_DATA}
  const actionSetToken = {type: constants.SET_TOKEN}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // save user data
  return getUser()
  .then(userData => {
    if (userData === null) {
      userData = {
        token: ''
      }
    }
    const { token } = userData
    dispatch(asyncReceiveSuccess(action))
    dispatch({...actionSetUser, userData})
    dispatch({...actionSetToken, token})

    return userData
  })
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

/**
 * set local data to show this is not user's first time opening the app
 */
export const setFirstTimeUser = () => dispatch => {
  const action = {type: constants.SET_FIRST_TIME_USER}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // save user data
  return setFirstTime()
  .then(() => {
    const firstTimeUser = false // user is no longer a first time user
    dispatch(asyncReceiveSuccess(action))
    dispatch({...action, firstTimeUser})
  })
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

/**
 * get first time user app status from local data
 */
export const getFirstTimeUser = () => dispatch => {
  const action = {type: constants.GET_FIRST_TIME_USER}
  const actionSetFirst = {type: constants.SET_FIRST_TIME_USER}
  // async action is ongoing
  dispatch(asyncRequest(action))
  // save user data
  return getFirstTime()
  .then(firstTimeUser => {
    dispatch(asyncReceiveSuccess(action))
    dispatch({...actionSetFirst, firstTimeUser})
  })
  .catch(error => dispatch(asyncReceiveError({...action, error: error.message})))
}

/**
 * signup a user
 * @param {object} formObject
 */
export const signUp = (formObject) => dispatch => {
  const action = {type: constants.SIGNUP_USER}
  // async action is ongoing
  dispatch(asyncRequest(action))

  return SIGNUP(formObject)
  .then(responseData => {
    const {status, message} = responseData
    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    const userData = responseData.data
    dispatch(asyncReceiveSuccess(action))
    dispatch(setUserData(userData))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

/**
 * sign a user in
 * @param {string} username
 * @param {string} password
 */
export const signIn = (username, password) => dispatch => {
  const action = {type: constants.SIGNIN_USER}
  // async action is ongoing
  dispatch(asyncRequest(action))

  return LOGIN(username, password)
  .then(responseData => {
    const {status, message} = responseData

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    const userData = responseData.data
    dispatch(asyncReceiveSuccess(action))
    dispatch(setUserData(userData))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

export const changePassword = (token, oldPassword, newPassword) => dispatch => {
  const action = {type: constants.CHANGE_PASSWORD}
  // async action is ongoing
  dispatch(asyncRequest(action))

  return UPDATE_PASSWORD(token, oldPassword, newPassword)
  .then(responseData => {
    const {status, message} = responseData

    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

export const updateUserProfile = (token, profile) => dispatch => {
  const action = {type: constants.PUT_USER_PROFILE}
  // async action is ongoing
  dispatch(asyncRequest(action))

  return UPDATE_USER_PROFILE(token, profile)
  .then(responseData => {
    const {status, message} = responseData

    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

/**
 * verify user's account with pin sent to mail
 * @param {string} token
 * @param {string} code
 */
export const verifyPin = (token, code) => dispatch => {
  const action = {type: constants.VERIFY_USER_PIN}
  const actionToken = {type: constants.SET_TOKEN}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return VERIFY_PIN(token, code)
  .then(response => {
    const token = response.headers.get('token')
    if (token) dispatch({...actionToken, token})
    return response.json()
  })
  .then(responseData => {
    const {status, message} = responseData

    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    dispatch(asyncReceiveSuccess(action))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

/**
 * send a new pin to the user via email
 * @param {string} token
 * @param {string} emailPhone
 */
export const resendPin = (token, emailPhone) => dispatch => {
  const action = {type: constants.RESEND_USER_PIN}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return RESEND_PIN(token, emailPhone)
  .then(responseData => {
    const {status, message} = responseData

    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    dispatch(asyncReceiveSuccess(action))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

/**
 * set's user's pin
 * @param {string} token
 * @param {string} pin
 */
export const setUserPin = (token, pin) => dispatch => {
  const action = {type: constants.SET_USER_PIN}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return SET_PIN(token, pin)
  .then(responseData => {
    const {status, message} = responseData

    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    dispatch(asyncReceiveSuccess(action))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

export const updateUserPin = (token, oldPin, newPin) => dispatch => {
  const action = {type: constants.UPDATE_USER_PIN}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return UPDATE_PIN(token, oldPin, newPin)
  .then(responseData => {
    const {status, message} = responseData

    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    dispatch(asyncReceiveSuccess(action))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

/**
 * Gets the profile of the logged in user
 * @param {string} token
 */
export const getUserProfile = (token, init = true) => dispatch => {
  const action = {type: constants.GET_USER_PROFILE}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return GET_PROFILE(token)
  .then(responseData => {
    const {status, message} = responseData
    if (!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }

    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    dispatch(asyncReceiveSuccess(action))
    const userData = init ? {...responseData.data, hasPIN: true, active: true, token} : responseData.data
    dispatch(setUserData(userData))

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

export const getUserDashboard = token => dispatch => {
  const action = {type: constants.GET_USER_DASHBOARD}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return GET_DASHBOARD(token)
  .then(responseData => {
    const {status, message, data} = responseData

    if(!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }
    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    const dashboard = data
    dispatch(asyncReceiveSuccess(action))
    dispatch({...action, dashboard})

    return responseData
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

export const getUserEarnings = token => dispatch => {
  const action = {type: constants.GET_USER_EARNINGS}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return GET_EARNINGS(token)
  .then(responseData => {
    const {status, message, data} = responseData

    if(!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }
    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    const earnings = data
    dispatch(asyncReceiveSuccess(action))
    dispatch({...action, earnings})

    return responseData;
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}

export const getUserOverallEarnings = token => dispatch => {
  const action = {type: constants.GET_USER_OVERALL_EARNINGS}
  // async action is ongoing
  dispatch(asyncRequest(action))
  return GET_OVERALL_EARNINGS(token)
  .then(responseData => {
    const {status, message, data} = responseData

    if(!status && message.name === 'TokenExpiredError'){
      AsyncStorage.removeItem('userData');
      const error = new Error(message.message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return;
    }
    if (!status) {
      const error = new Error(message)
      dispatch(asyncReceiveError({...action, error: error.message}))
      return responseData
    }
    const overall_earnings = data
    dispatch(asyncReceiveSuccess(action))
    dispatch({...action, overall_earnings})

    return responseData;
  })
  .catch(error => {
    dispatch(asyncReceiveError({...action, error: error.message}))
    return error
  })
}
