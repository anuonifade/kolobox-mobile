//This file used for Earning. All the Earning UI's rendered through it
import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import { Actions } from 'react-native-router-flux';
import { AccountHeader } from '../common';
import EarningsTabs from './EarningsTabs';

class InvestmentPage extends Component {
  static navigationOptions = {
    tabBarLabel: 'Earnings',
    tabBarIcon: () => (
      <Image
      style={{ height: 22,
      width: 22,
    }} source={require('../img/n_white.png')}
      />)
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
      <AccountHeader showHelp showWallet headerTxt={'Kolobox'} />
      <EarningsTabs />
      </View>
    );
  }
}

const Styles = {
    HeaderImage: {
    height: 25,
    width: 25,
    padding: 10
  },
    imageView: {
    justifyContent: 'center',
    height: 40,
    flexDirection: 'row',
    margin: 5
  },
    textStyles: {
    fontSize: 14,
    color: '#000000',
    padding: 5,
  }
};
export default InvestmentPage;
