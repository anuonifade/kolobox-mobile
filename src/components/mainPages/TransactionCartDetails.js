import React, { Component } from 'react';
import { Text, View, ScrollView, TouchableOpacity } from 'react-native';
import TransactionCardDetailsRow from './TransactionCardDetialRow';

export default class TransactionCartDetails extends Component {

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>

        <ScrollView >
      <View >


<View style={{ flexDirection: 'row', marginLeft: 5, marginRight: 5, marginTop: 5, alignItems: 'center' }}>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Date' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Description' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Debits' } </Text>
</View>
<View style={{ flex: 1, alignItems: 'center' }}>
<Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Credits' } </Text>
</View>
</View>
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />
<TransactionCardDetailsRow />

</View>
  </ScrollView>
  <Text style={[styles.textStyles, { color: '#000000', padding: 0, alignSelf: 'center' }]}> {'Swipe left and right to view round up and actual amount' } </Text>

  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end', marginBottom: 20 }}>
              <TouchableOpacity
              style={[styles.buttonStyle, { alignItems: 'center',
               justifyContent: 'center' }]}
              >
              <Text style={[{ color: '#ffffff' }]}>Round Up</Text>
              </TouchableOpacity>
              </View>
              </View>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    borderColor: '#4991bf',
    borderWidth: 2,
    margin: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#4991bf',
    padding: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
   textStyles: {
    fontSize: 16,
    color: '#ffffff',
    padding: 5,
    flex: 1,
    justifyContent: 'flex-start',
  },
  blackTextStyles: {
   fontSize: 16,
   color: '#000000',
 },
  interestContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#4991bf',
      padding: 5
  },
  accountContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
      padding: 5
  },
  rightViewStyle: {
        flex: 1,
        alignItems: 'flex-start',
      justifyContent: 'flex-start'
  },
  leftViewStyle: {
      flex: 1,
        alignItems: 'flex-start',
      justifyContent: 'flex-start'
  },
  centerViewStyle: {
      flex: 1,
        alignItems: 'center',
      justifyContent: 'center'
  },
  arrowImageView: {

      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      flexDirection: 'row'
  },
  horizontalLineStyle: {
    height: 60,
    width: 2,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'gray'
  },
  buttonStyle: {
    height: 30,
    width: 120,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb',
  },
  benefitsContainerStyle: {
   flexDirection: 'row',
   margin: 10,
   padding: 0
 },
 lineStyle: {
   height: 2,
   backgroundColor: '#2b78e4',
   flex: 1,
   marginTop: 10,
     alignItems: 'center',
       justifyContent: 'center',
 },
 benefitTextStyle: {
   fontSize: 16,
   color: '#2b78e4',
     paddingBottom: 10,
     paddingLeft: 5,
     paddingRight: 5,
   marginBottom: 10,
   alignItems: 'center',
   justifyContent: 'center',
 },
 blacklineStyle: {
   height: 2,
   backgroundColor: '#2b78e4',
   flex: 1,
   marginTop: 10,
     alignItems: 'center',
       justifyContent: 'center',
 },
 debitCardTextStyle: {
   fontSize: 16,
   color: '#000000',
   flex: 1
 },
 arrowImageStyle: {
   height: 25,
   width: 25,
   padding: 5,
   justifyContent: 'flex-end'
 },
 graphTextStyle: {
   fontSize: 16,
   color: '#000000',
   margin: 10,
   padding: 10
 }


};
