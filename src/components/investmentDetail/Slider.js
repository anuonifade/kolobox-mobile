// This file used for inflate rangebar on investment screen
import React from 'react'
import Slider from 'react-native-slider'
import { StyleSheet, View, Text } from 'react-native'

class SliderExample extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      value: 0
    }
  }

  componentWillMount () {
    this.setState({ value: this.props.value })
  }

  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <View style={styles.container}>
          <Slider
            value={this.state.value}
            maximumValue={this.props.maxval}
            minimumTrackTintColor={'#000000'}
            maximumTrackTintColor={'#000000'}
            thumbTintColor={'#2b78e4'}
            onValueChange={value => this.setState({ value })}
          />
        </View>
        <View style={styles.selectedPriceTextStyle}>
          <View style={{ borderBottomWidth: 2 }}>
            <Text>
              {parseInt(this.state.value, 10)}
            </Text>
          </View>
        </View>
      </View>

    )
  }
}
// Styles used for View adjustment

const styles = StyleSheet.create({
  container: {
    flex: 8,
    marginLeft: 10,
    marginRight: 10,
    justifyContent: 'center',
    padding: 0
  },
  selectedPriceTextStyle: {
    borderBottomColor: '#000000',

    flex: 2,
    paddingBottom: 0,
    alignItems: 'center'
  }
})

export default SliderExample
