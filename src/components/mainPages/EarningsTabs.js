//This file used for inflate Earning Tabs
import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import {
  createNavigator,
  createNavigationContainer,
  TabRouter,
  addNavigationHelpers
} from 'react-navigation';
import EarningList from './EarningList';
import EarningOverallReport from './EarningOverallReport';
// const used for navigate tab button

const CustomTabBar = ({ navigation }) => {
  const { routes } = navigation.state;
  return (
    <View style={styles.tabContainer}>
      {routes.map(route => (
        <TouchableOpacity
          onPress={() => navigation.navigate(route.routeName)}
          style={styles.tab}
          key={route.routeName}
        >
          <Text>{route.routeName}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};
//used for create custom tabs

const CustomTabView = ({ router, navigation }) => {
  const { routes, index } = navigation.state;
  const ActiveScreen = router.getComponentForState(navigation.state);
  const routeNav = addNavigationHelpers({
    ...navigation,
    state: routes[index],
  });
  const routeOptions = router.getScreenOptions(routeNav, 'tabBar');
console.log(routeOptions.headerTintColor);
  return (
    <View style={styles.container}>
      <CustomTabBar navigation={navigation} />
      <ActiveScreen
        navigation={addNavigationHelpers({
          ...navigation,
          state: routes[index],
        })}
      />
    </View>
  );
};

// used for tab routing

const CustomTabRouter = TabRouter(
  {
    List: {
      screen: EarningList,
      path: '',
    },
    'Earning History': {
      screen: EarningOverallReport,
      path: '',
    },
  },
  {

    initialRouteName: 'List',

  },
);

const InvestmentTabs = createNavigationContainer(
  createNavigator(CustomTabRouter)(CustomTabView)
);

const styles = StyleSheet.create({
  container: {
    flex: 1
    //marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  tabContainer: {

    flexDirection: 'row',
    height: 48,
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 4,
    borderWidth: 1,
    borderColor: '#ddd',
    backgroundColor: 'white'
  },
});

export default InvestmentTabs;
