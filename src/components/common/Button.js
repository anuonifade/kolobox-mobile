import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onpress, children }) => (
  <TouchableOpacity onPress={onpress} style={styles.buttonStyle}>
    <Text style={styles.textStyle}>
      {children}
    </Text>
  </TouchableOpacity>

);

const styles = {
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#fff',
    borderColor: '#007aff',
    marginRight: 5,
    marginLeft: 5,
    position: 'relative'
  },
  textStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  }
};

export { Button };
