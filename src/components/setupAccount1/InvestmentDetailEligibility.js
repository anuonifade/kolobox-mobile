import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import { AccountHeader } from '../common';

class InvestmentDetailEligibility extends Component {

  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
      <AccountHeader headerTxt="Kolonaija" />
      <ScrollView>
      <Text style={Styles.textStyles} >Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut</Text>

    <View style={{ margin: 10 }} >
    <View style={{ flexDirection: 'row' }} >
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </Text>
    </View>
    <View style={{ flexDirection: 'row' }}>
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
    </View>
    <View style={{ flexDirection: 'row'}}>
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</Text>
    </View>
    <View style={{ flexDirection: 'row'}}>
      <Text style={Styles.bulletStyle}>{'\u2022'}</Text>
      <Text style={Styles.bulletTextStyle}>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
    </View>
    </View>
        </ScrollView>
      </View>

    );
  }

}

const Styles = {

  textStyles: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
},
 benefitsContainerStyle: {
  flexDirection: 'row',
  margin: 10
},
lineStyle: {
  height: 2,
  backgroundColor: '#2b78e4',
  flex: 1,
    alignItems: 'center'
},
benefitTextStyle: {
  flex: 1,
  fontSize: 16,
  color: '#2b78e4',
  padding: 5,
  alignItems: 'center',
  justifyContent: 'center',
},
bulletStyle: {
  fontSize: 27,
  color: '#000000',

},
bulletTextStyle: {
  fontSize: 16,
  color: '#000000',
  margin: 5,
}
};

export default InvestmentDetailEligibility;
