import React, { Component } from 'react';
import { View, TextInput, Image } from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import Icon from 'react-native-vector-icons/FontAwesome';
import DownArrow from '../img/down_arrow.png';

class MonthSelector extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textInputValue: ''
    };
  }

  render() {
    let index = 0;
    const data = [
      { key: index++, section: true, label: 'Month' },
      { key: index++, label: '01' },
      { key: index++, label: '02' },
      { key: index++, label: '03' },
      { key: index++, label: '04' },
      { key: index++, label: '05' },
      { key: index++, label: '06' },
      { key: index++, label: '07' },
      { key: index++, label: '08' },
      { key: index++, label: '09' },
      { key: index++, label: '10' },
      { key: index++, label: '11' },
      { key: index++, label: '12' }

    ];

    return (
      <View style={{ flexDirection: 'column' }}>
        <ModalSelector
          data={data}
          initValue='01'
          supportedOrientations={['portrait']}
          onChange={(option) => {
            this.setState({ textInputValue: option.label });
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <Icon
              name={'credit-card'}
              size={24}
              color='grey'
              style={{ padding: 2 }}
            />
            <TextInput
              style={{
                borderBottomWidth: 2,
                borderColor: '#D3D3D3',
                width: 80,
                marginLeft: 10,
                height: 30,
                fontSize: 14,
                alignSelf: 'center'
              }}
              editable={false}
              placeholder='Month'
              value={this.state.textInputValue}
            />
            <Image
              style={Styles.arrowImageStyle}
              source={DownArrow}
            />
          </View>
        </ModalSelector>
      </View>
    );
  }
}

const Styles = {
  arrowImageStyle: {

  }
};

export default MonthSelector;
