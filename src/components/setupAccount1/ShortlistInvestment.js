import React, { Component } from 'react';
import { ScrollView, View, Text, Alert } from 'react-native';
// import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import NameOfInvestmentDetail from '../mainPages/NameOfInvestmentDetail';
import { Spinner } from '../common/spinner';
import {
  getInvestmentApproach,
  getMyInvestmentApproach,
  addToShortlist,
  removefromShortlist
} from '../../actions/investment-actions';

class ShortlistInvestment extends Component {
  constructor(props) {
    super(props);
    this.data = null;
    this.state = {
      loading: false,
      investment: {
        ProductCategories: [],
        shortlisted: {
          product: []
        }
      }
    };
  }

  componentWillMount() {
    const { getMyInvestmentApproach } = this.props;

    getMyInvestmentApproach()
    .then(investment => this.loadInvestmentOptions(investment));
  }

  loadInvestmentOptions(investment) {
    const { getInvestmentApproach, token } = this.props;
    const hasInvestment = !!investment;
    if (!hasInvestment) {
      return;
    }

    this.setState({ loading: true });
    getInvestmentApproach(token, investment.id)
    .then(({ status, data, message }) => {
      if (!status) {
        Alert.alert('Error', message, [{ text: 'Try again',
          onPress: () => {
            this.setState({ loading: false });
            this.loadInvestmentOptions(investment.id);
          } }]);
        return;
      }
      this.setState({
        investment: data,
        loading: false
      });
    });
  }

  showInvestmentProducts(product, index) {
    const { shortlisted } = this.props;
    const checked = !!shortlisted[product.id];
    return (
      <NameOfInvestmentDetail
        checked={checked}
        product={product}
        key={index}
      />
    );
  }

  renderRadio() {
    const { shortlisted } = this.props;
    const emptyView = (
      <View style={styles.renderRadio}>
        <Text style={{ fontSize: 16 }}> You have not selected any product.</Text>
        <Text style={{ fontSize: 12, color: '#3B90E3', top: 10 }}>
          No product shortlisted
        </Text>
      </View>
    );
    const investmentView = [];
    if (shortlisted.product && shortlisted.product.length !== 0) {
      shortlisted.product
        .forEach((product, index) =>
        investmentView.push(this.showInvestmentProducts(product, index))
      );
    }
    return investmentView.length === 0
      ? emptyView : investmentView;
  }

  renderInvestmentView() {
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        {this.renderRadio()}
      </View>
    );
  }

  render() {
    const styledSpinner = <View style={styles.styledSpinner}><Spinner /></View>;

    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <ScrollView>
          <View style={{ flex: 1, top: 10 }}>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              {this.state.loading ? styledSpinner : this.renderRadio()}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { token } = state.user;
  const { shortlisted } = state.investment;
  return {
    token,
    shortlisted
  };
};

const mapDispatchToProps = ({
  getInvestmentApproach,
  getMyInvestmentApproach,
  addToShortlist,
  removefromShortlist
});

const styles = {
  styledSpinner: {
    marginTop: 50,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  renderRadio: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShortlistInvestment);
