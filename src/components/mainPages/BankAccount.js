//this file used for Show Bank Account Info
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert, AsyncStorage, } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
// import Prompt from 'react-native-prompt'
import { Spinner } from '../common';

export default class DebitCardAdd extends Component {
  constructor(props) {
    super(props);
    this.data = null;
    this.state = {
      switch1Value: false,
      switch2Value: false,
      removing: false,
      promptVisible: false,
      userpin: '',
      modifiedUserDataArr: [],
      isDataParsed: false
    };
  }
  //Used for set toggle button state
  toggleSwitch1 = (value) => {
    this.setState({ switch1Value: value });
  }
  toggleSwitch2 = (value) => {
    this.setState({ switch2Value: value });
  }
  toggleSwitch3 = (value) => {
    this.setState({ switch3Value: value });
  }
  toggleSwitch4 = (value) => {
    this.setState({ switch4Value: value });
  }
  toggleStatus() {
    this.setState({
      status: !this.state.status
    });
  }
  updateUserBankAccount(bankdata) {
    console.log('update User acc called');
    AsyncStorage.getItem('userData', (err, result) => {
      // this.data = JSON.parse(result)
      this.setState({ modifiedUserDataArr: JSON.parse(result), isDataParsed: true });
      this.data = this.state.modifiedUserDataArr;
      // console.log(this.state.isDataParsed)
      if (this.state.isDataParsed) {
        console.log('data parsed');
        this.data.UserBankAccounts = bankdata;
        // console.log(this.data)
        AsyncStorage.setItem('userData', JSON.stringify(this.data), () => {
          this.props.handler();
        });
      }
    });
  }
  deleteBankApi(value) {
    Prompt(
      'Please Enter the pin',
      '4 digit pin',
      [
        { text: 'Cancel', onPress: () => this.setState({ promptVisible: false }) },
        {
          text: 'OK', onPress: (value) => {
            this.deleteBankApi(value);
          }
        },
      ],
      {
        cancelable: false,
        defaultValue: 'test',
        placeholder: 'placeholder'
      }
    );
    AsyncStorage.getItem('userData', (err, result) => {
      this.data = JSON.parse(result);
      this.setState({ removing: true });
      // console.log(value)
      fetch(GLOBALS.BASE_URL + 'user/me/bank/' + this.props.bankAccounts.id, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.data.token,
        },
        body: JSON.stringify({
          pin: value
        })
      }).then((response) => response.json())
        .then(responseJson => {
          // console.log(responseJson)
          if (responseJson.status !== true && responseJson.message.name === 'TokenExpiredError') {
            AsyncStorage.removeItem('userData', (error) => {
              if (error !== null) {
                console.log('unable to log out');
                console.log(err);
              }
              Actions.loginPages();
            });
          } else {
            this.setState({ removing: false });
            if (responseJson.status === true) {
              Alert.alert(
                'Success',
                '',
                [
                  { text: 'OK', onPress: () => this.updateUserBankAccount(responseJson.data) },
                ],
                { cancelable: false }
              );
            } else {
              this.setState({ removing: false });
              Alert.alert(
                'Error!',
                responseJson.message,
                [
                  { text: 'OK' },
                ],
                { cancelable: false }
              );
            }
          }
        })
        .catch((error) => {
          this.setState({ removing: false })
          console.error(error)
        })
    })
  }
  renderButton() {
    if (!this.state.removing) {
      return (
        <View style={(this.props.hideRemoveBtn) ? { display: 'none' } : ''} >
          <TouchableOpacity onPress={() => this.setState({ promptVisible: true })}>
            <Text style={[Styles.ForgotText, { color: '#2b78e4', textDecorationLine: 'underline', }]} >Remove</Text>
          </TouchableOpacity>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          </View>
        </View>
      )
    } else {
      return (
        <Spinner />
      )
    }
  }

  renderprompt() {
  }

  render() {
    const { cover } = Styles
    return (
      <View style={cover}>
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View
            style={{ flex: 1, flexDirection: 'column', borderRadius: 5, borderWidth: 1, borderColor: '#000000', margin: 10, paddingLeft: 5, paddingBottom: 5 }}>
            <Icon size={25} color='blue' style={{ padding: 2, height: 25, marginTop: 5, alignSelf: 'flex-end' }} />
            <Text style={Styles.ForgotText} >{this.props.bankAccounts.bank_name}</Text>
            <Text style={Styles.ForgotText} >{this.props.bankAccounts.account_number}</Text>
            <Text style={Styles.ForgotText} >{this.props.bankAccounts.account_name}</Text>
          </View>
          <View style={Styles.forgotButton}>
            {this.renderButton()}
          </View>
        </View>
      </View>
    );
  }
}
const Styles = {
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },
  forgotButton: {
    flex: 1,
    alignItems: 'center',
  },
  ForgotText: {
    fontSize: 16,
    color: '#000000',
  },
  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center',
  },
  InputStyle: {
    marginTop: 12,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0,
    height: 25,
    borderBottomWidth: 2,
    borderColor: '#D3D3D3'
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 10,
    padding: 0,
    flex: 1
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1
  },
  forgotButton: {
    flex: 1,
    alignItems: 'center'
  }
}
