import React from 'react'
import { View } from 'react-native'
import { Header } from '../common'
import CustomTabs from './signin'

const LoginPage = () => {
  return (
    <View style={{ flex: 1 }}>
      <Header showHelp headerTxt={'Kolobox'} />
      <CustomTabs />
    </View>
  );
};

export default LoginPage;
