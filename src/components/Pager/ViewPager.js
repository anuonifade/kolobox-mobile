import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';

import { FullWidthButton } from '../common';
import { setFirstTimeUser } from '../../actions/user-actions';

export class PageSwiper extends Component {
  render() {
    const gotokolobox = () => {
      const { setFirstTimeUser } = this.props;
      setFirstTimeUser()
      .then(() => {});
      Actions.loginPages();
    };
    return (
      <Swiper
        style={styles.wrapper}
        showsButtons={false}
        dotStyle={styles.dotStyles}
        activeDotStyle={styles.activeDotStyle}
        activeDotColor={'#007aff'}
      >
        <View style={styles.slide1}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.text}>
              How kolobox works?
            </Text>
            <Text style={[styles.text, { alignSelf: 'flex-start', fontSize: 18, margin: 10 }]}>
              Making investment a daily routine!.
            </Text>
            <Text style={styles.detailText}>
              Get a healthy, risk averse return on your investments -
              start investing little and often. Smart, simple, hassle-free.
              Create an account for FREE, set how much and how frequent
              you want to constantly invest into your kolobox account.
              With kolobox you can start investing in Minutes
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <FullWidthButton onPress={gotokolobox} Title='Try Kolobox' />
          </View>
        </View>
        <View style={styles.slide1}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.text}>
              Find out More
            </Text>
            <Text style={[styles.text, { alignSelf: 'flex-start', fontSize: 18, margin: 10 }]}>
              Help your investments grow
            </Text>
            <Text style={styles.detailText}>
              Investing your spare change into investment
              funds can be a great way to grow your money,
              and can offer higher long term returns than traditional saving
              products
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <FullWidthButton onPress={gotokolobox} Title='Sign up Now' />
          </View>
        </View>
        <View style={styles.slide1}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.text}>
              What are the options?
            </Text>
            <Text style={[styles.text, { alignSelf: 'flex-start', fontSize: 18, margin: 10 }]}>
              Investments Made easy.
            </Text>
            <Text style={styles.detailText}>
              Kolobox is a platform that works directly with Radix Capital Partners
              to enable you to invest your spare change.
              Radix Capital Partners is an Investment/Asset Management
              Firm regulated by the Securities and Exchange Commission (SEC).
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <FullWidthButton onPress={gotokolobox} Title='Explore all' />
          </View>
        </View>
        <View style={styles.slide1}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.text}>
              Our Fees
            </Text>
            <Text style={[styles.text, { alignSelf: 'flex-start', fontSize: 18, margin: 10 }]}>
              Affordable for everyone
            </Text>
            <Text style={styles.detailText}>
              Kolobox is completely free.
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <FullWidthButton onPress={gotokolobox} Title='Try Kolobox' />
          </View>
        </View>
        <View style={styles.slide1}>
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.text}>
              Security and Safety
            </Text>
            <Text style={[styles.text, { alignSelf: 'flex-start', fontSize: 18, margin: 10 }]}>
              Your Security Is Our Only Priority.
            </Text>
            <Text style={styles.detailText}>
              Kolobox uses the highest levels of Internet Security,
              and it is secured by 256 bits SSL security encyption to ensure
              that your information is completely protected from fraud.
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <FullWidthButton onPress={gotokolobox} Title='Where do I signup' />
          </View>
        </View>
      </Swiper>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
    backgroundColor: '#ffffff'
  },
  text: {
    color: '#000000',
    fontSize: 20,
    fontWeight: 'bold',
    lineHeight: 30
  },
  detailText: {
    color: '#000000',
    fontSize: 14,
    margin: 10,
    lineHeight: 35
  },
  dotStyles: {
    backgroundColor: 'rgba(0,0,0,.2)',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 30
  },
  activeDotStyle: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 30
  }
});

const mapDispatchToProps = ({
  setFirstTimeUser
});

export default connect(null, mapDispatchToProps)(PageSwiper);
