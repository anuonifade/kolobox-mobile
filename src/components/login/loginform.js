// {this is a Login form to login into user Account}
import React, { Component } from 'react';
import { View, ScrollView, Text, TouchableOpacity, Image, Alert, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { signIn } from '../../actions/user-actions';
import { Input, FullWidthButton, Spinner } from '../common';

export class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      loading: false
    };
  }

  authUser = () => { // eslint-disable-line no-undef
    this.setState({ loading: true });
    const { username, password } = this.state;
    const { signIn } = this.props;

    signIn(username, password)
    .then(({ status, data, message }) => {
      if (!status) {
        Alert.alert('Authentication Error', message,
          [{ text: 'close', onPress: () => this.setState({ loading: false }) }]
        );
        return;
      }
      this.setState({
        username: '',
        password: '',
        loading: false
      });

      AsyncStorage.setItem('userData', JSON.stringify(data), () => {
        console.log('User Data saved');
        Actions.mainPages();
      });
    })
    .catch(error => {
      Alert.alert('Error', error.message,
        [{ text: 'close', onPress: () => this.setState({ loading: false }) }]
      );
    });
  }

  // render button
  renderButton() {
    if (this.state.loading) {
      return <Spinner />;
    }
    return <FullWidthButton onPress={this.authUser} Title={'Submit'} />;
  }

  render() {
    const { cover, forgotButton, ForgotText, HeaderImage } = Styles;
    return (

      <ScrollView style={cover}>
        <View style={{ flex: 1 }}>
          <Input
            label={'envelope'}
            onChangeText={username => this.setState({ username })}
            placeholder={'Email'}
            keyboardType={'email-address'}
            value={this.state.username}
          />
          <Input
            label={'lock'}
            onChangeText={password => this.setState({ password })}
            placeholder={'Password'}
            secureTextEntry
            keyboardType={'default'}
            value={this.state.password}
          />
          <View style={forgotButton}>
            <TouchableOpacity onPress={Actions.forgotPassword}>
              <Text style={ForgotText}>
                Forgot Password?
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 30, marginBottom: 30 }}>
            <Image style={HeaderImage} source={require('../img/quoteimg.png')} />
          </View>
          <View style={{ marginTop: 15, flex: 1 }}>
            {this.renderButton()}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const Styles = {
  cover: {
    flex: 1,
    paddingLeft: 8,
    paddingRight: 8
  },
  forgotButton: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 10
  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  },
  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center'
  }
};

const mapDispatchToProps = ({
  signIn
});

export default connect(null, mapDispatchToProps)(LoginForm);
