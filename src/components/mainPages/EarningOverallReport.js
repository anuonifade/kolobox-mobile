//This UI used for Earning Overall Report
import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import Moment from 'moment';
import EarningOverallReportRow from './EarningOverallReportRow';
import { getUserOverallEarnings } from '../../actions/user-actions';

class EarningsPerformance extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      error: '',
      overallEarning: {
        savings: {
          total_savings: 0,
          average_savings: 0
        },
        earnings: [
          {
            date: '1979-01-01',
            amount: 0,
            name: '',
            interest: 0
          }
        ]
      }
    };
  }

  componentWillMount() {
    const { userData, getUserOverallEarnings } = this.props;
    this.setState({ loading: true });

    getUserOverallEarnings(userData.token)
      .then((overallEarnings) => {
        this.setState({
          loading: false,
          overallEarning: overallEarnings.data
        });
      }).catch((error) => {
        this.setState({
          loading: false,
          error: 'There is no overall earnings to load'
        });
        console.log(error);
      });
  }

  renderOverallEarnings = (overallEarnings) => {
    if (this.state.error && this.state.error !== '') {
      return (
        <Text
          style={{
            fontWeight: 'bold',
            top: 10,
            alignSelf: 'center',
            alignItems: 'center'
          }}
        >
          { this.state.error}
        </Text>
      );
    }
    if (overallEarnings.earnings.length === 0) {
      return (
        <Text
          style={{
            fontWeight: 'bold',
            top: 10,
            alignSelf: 'center',
            alignItems: 'center'
          }}
        >
          You do not have any earnings at this time
        </Text>
      );
    }
      return (overallEarnings
        .earnings
        .map((overallEarning, index) =>
          (<EarningOverallReportRow
              data={overallEarning || []}
              key={index}
          />))
        );
  }

  render() {
    Moment.locale('en');
    return (
      <ScrollView style={{ flex: 1 }}>
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
            <View style={[styles.leftViewStyle, { marginLeft: 5 }]}>
              <Text style={styles.blackTextStyles}>
                INVESTMENT: ₦{
                  parseFloat(this.state.overallEarning.savings.total_savings)
                    .toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') || 0.00
                }
              </Text>
              <Text style={styles.blackTextStyles}>
                MONTHLY AVERAGES: ₦{
                  parseFloat(this.state.overallEarning.savings.average_savings)
                    .toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') || 0.00
                }
              </Text>
            </View>
          </View>
          <View style={{ alignItems: 'center', margin: 5 }}>
            <Text style={styles.blackTextStyles}>
              {
                this.state.overallEarning.earnings
                && this.state.overallEarning.earnings.length > 0
                ? `Upto ${Moment(this.state.overallEarning.earnings[0].date).format('DD MMM YY')}`
                : '0000-00-00'
              }
            </Text>
          </View>
          <View
            style={[styles.benefitsContainerStyle,
              { marginLeft: 10, marginRight: 10, padding: 0 }]
            }
          >
            <Text style={styles.lineStyle} />
            <Text style={styles.benefitTextStyle} >Reports</Text>
            <Text style={styles.lineStyle} />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 5,
              marginRight: 5,
              marginTop: 5,
              alignItems: 'center'
            }}
          >
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text style={[styles.textStyles, { color: '#000000', padding: 0 }]}> {'Date' } </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text
                style={[styles.textStyles,
                  { color: '#000000', padding: 0 }]}
              > {'Investment Name' } </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text
                style={[styles.textStyles,
                  { color: '#000000', padding: 0 }
                ]}
              > {'Amount' } </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text
                style={[styles.textStyles,
                  { color: '#000000', padding: 0 }
                ]}
              > {'Interest' } </Text>
            </View>
          </View>
        </View>
          { this.state.loading
            ? <ActivityIndicator size="large" color="#1034A6" />
            : this.renderOverallEarnings(this.state.overallEarning)
          }
      </ScrollView>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignSelf: 'center',
    borderColor: '#4991bf',
    borderWidth: 2,
    margin: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#4991bf',
    padding: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
   textStyles: {
    fontSize: 16,
    color: '#ffffff',
    padding: 5,
    flex: 1,
    justifyContent: 'flex-start',
  },
  blackTextStyles: {
   fontSize: 16,
   color: '#000000',
 },
  interestContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#4991bf',
    padding: 5
  },
  accountContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 5
  },
  rightViewStyle: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  leftViewStyle: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  centerViewStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  arrowImageView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  horizontalLineStyle: {
    height: 60,
    width: 2,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: 'gray'
  },
  buttonStyle: {
    height: 30,
    width: 120,
    marginBottom: 5,
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: '#3d93cb',
    borderColor: '#3d93cb',
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 10,
    padding: 0
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  benefitTextStyle: {
    fontSize: 16,
    color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  blacklineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
  graphTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
    padding: 10
  }
};

const mapStateToProps = (state) => {
  const { userData, overall_earnings } = state.user;
  return {
    userData,
    overall_earnings
  };
};

const mapDispatchToProps = ({
  getUserOverallEarnings
});

export default connect(mapStateToProps, mapDispatchToProps)(EarningsPerformance);
