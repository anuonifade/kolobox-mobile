import React from 'react'
import { View, ActivityIndicator } from 'react-native'

const Spinner = ({ size }) => (
  <View style={Styles.spinnerStytle} >
    <ActivityIndicator size={size || 'large'} />
  </View>
)
const Styles = {
  spinnerStytle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
}
export { Spinner }
