// This file used for Basic info of user
import React, { Component } from 'react'
import { View, Text, TextInput, TouchableOpacity, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import { Spinner } from '../common'
import DatePicker from '../common/datePicker'

import { updateUserProfile } from '../../actions/user-actions'

export class BasicInfo extends Component {
  constructor () {
    super()
    this.state = {
      loading: false
    }
  }

  componentDidMount () {
    const { userData } = this.props
    const { email, firstname, lastname, dob, occupation, phone, token } = userData
    this.setState({
      email, firstname, lastname, dob, occupation, phone, token
    })
  }

  handleToUpdate (val) {
    this.setState({ dobirth: val })
  }

  // method used for update user info
  updateBasicInfo () {
    if (this.state.firstname === '' || this.state.lastname === '' ||
      this.state.phone === '' || this.state.email === '' ||
      this.state.password === '' || this.state.dob === '' ||
      this.state.occupation === '') {
      Alert.alert(
        'Error!',
        'Please Fill all the fields',
        [
          { text: 'OK', onPress: () => { } }
        ],
        { cancelable: false }
      )
      return
    }

    const { updateUserProfile } = this.props
    const { token, loading, ...profile } = this.state
    this.setState({ loading: true })

    updateUserProfile(token, profile)
    .then(({status, message, data}) => {
      if (!status) {
        Alert.alert(
          'Error',
          message,
          [
            { text: 'OK', onPress: () => this.setState({ loading: false }) }
          ],
          { cancelable: false }
        )
        return
      }

      Alert.alert(
        'Success!',
        'Profile details updated',
        [
          { text: 'OK', onPress: () => this.setState({ loading: false }) }
        ],
        { cancelable: false }
      )
    })
    .catch(error => {
      Alert.alert(
        'Error',
        error.message,
        [
          { text: 'OK', onPress: () => this.setState({ loading: false }) }
        ],
        { cancelable: false }
      )
      throw error
    })
  }

  // used for render update button
  renderButton () {
    if (!this.state.updating) {
      return (
        <TouchableOpacity onPress={this.updateBasicInfo.bind(this)} style={{ alignItems: 'center' }}>
          <Text style={[Styles.ForgotText]}>
            Update
          </Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <Spinner />
      )
    }
  }

  renderMainScreen () {
    const { cover } = Styles
    return (
    this.state.loading ? <Spinner />
    : <View style={cover}>
      <View style={{ flex: 1, flexDirection: 'column', paddingBottom: 15 }}>
        <View style={Styles.benefitsContainerStyle}>
          <TextInput
            onChangeText={firstname => this.setState({ firstname })}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            placeholder='First Name'
            placeholderTextColor={'#D3D3D3'}
            value={this.state.firstname}
            style={[Styles.InputStyle, { flex: 1 }]} />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <TextInput
            onChangeText={lastname => this.setState({ lastname })}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            placeholder='Last Name'
            placeholderTextColor={'#D3D3D3'}
            value={this.state.lastname}
            style={[Styles.InputStyle, { flex: 1 }]} />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <TextInput
            onChangeText={email => this.setState({ email })}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            placeholder='Email'
            placeholderTextColor={'#D3D3D3'}
            value={this.state.email}
            style={[Styles.InputStyle, { flex: 1 }]} />
        </View>
        <DatePicker
          label={'calendar'}
          handleToUpdate={this.handleToUpdate.bind(this)}
          placeholder={'Date of Birth'}
          dob={this.state.dob}
          date={this.state.dob}
           />
        <View style={Styles.benefitsContainerStyle}>
          <TextInput
            onChangeText={occupation => this.setState({ occupation })}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            placeholder='Occupation'
            placeholderTextColor={'#D3D3D3'}
            value={this.state.occupation}
            style={[Styles.InputStyle, { flex: 1 }]} />
        </View>
        <View style={Styles.benefitsContainerStyle}>
          <TextInput
            onChangeText={phone => this.setState({ phone })}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            placeholder='Phone(Min. 11 Digits)'
            placeholderTextColor={'#D3D3D3'}
            value={this.state.phone}
            style={[Styles.InputStyle, { flex: 1 }]} />
        </View>
        {this.renderButton()}
      </View>
    </View>
    )
  }
  render () {
    return (
    this.renderMainScreen()
    )
  }
}
const Styles = {
  cover: {
    flex: 1,
    flexDirection: 'column',
    paddingLeft: 8,
    paddingRight: 8
  },
  forgotButton: {
    flex: 1,
    alignItems: 'center'

  },
  ForgotText: {
    fontSize: 16,
    color: '#2b78e4',
    textDecorationLine: 'underline'
  },
  HeaderImage: {
    height: 85,
    width: 286,
    alignSelf: 'center'
  },
  InputStyle: {
    marginTop: 12,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 16,
    paddingBottom: 0,
    height: 25

  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 5,
    padding: 0,
    borderBottomWidth: 2,
    borderColor: '#D3D3D3'
  },
  blackTextStyles: {
    fontSize: 16,
    color: '#3d93cb',
    borderBottomWidth: 2,
    borderColor: '#3d93cb'
  }

}

const mapStateToProps = ({ user: {userData} }) => ({userData});

const mapDispatchToProps = ({
  updateUserProfile
})

export default connect(mapStateToProps, mapDispatchToProps)(BasicInfo)
