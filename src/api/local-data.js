import { AsyncStorage } from 'react-native'

/**
 * retrives user's data from the database
 */
export const getUserData = () => {
  return AsyncStorage.getItem('userData')
  .then(result => {
    if (result === null) {
      return result
    }
    const userData = JSON.parse(result)
    return userData
  })
}

/**
 * gets the current logged-in user's token
 */
export const getUserToken = () => {
  return AsyncStorage.getItem('userData')
  .then(result => {
    if (result === null) {
      return result
    }
    const userData = JSON.parse(result)
    return userData.token
  })
}

/**
 * user data object
 * @param {object} data
 */
export const saveUserData = data => AsyncStorage.setItem('userData', JSON.stringify(data)).then(() => true)

/**
 * Set user investment approach
 * @param {object} data
 */
export const setInvestmentApproach = data => AsyncStorage.setItem('investmentApproach', JSON.stringify(data)).then(() => true)

/**
 * get user's investment approach
 */
export const getInvestmentApproach = () => {
  return AsyncStorage.getItem('investmentApproach')
  .then(result => {
    if (result === null) {
      return result
    }
    const investmentApproach = JSON.parse(result)
    return investmentApproach
  })
}

/**
 * removes user's data
 */
export const removeUserData = () => AsyncStorage.removeItem('userData').then(() => true)

/**
 * checks if it's a first time user
 */
export const getFirstTimeUser = () => {
  return AsyncStorage.getItem('firstTimeUser')
  .then(result => {
    if (result === null) {
      return true
    }
    return false
  })
}

/**
 * checks if it's a first time user
 */
export const setFirstTimeUser = () => AsyncStorage.setItem('firstTimeUser', 'false').then(() => true)

/**
 * get shortlisted products by user
 */
export const getShortlistedProducts = () => {
  return AsyncStorage.getItem('shortlisted')
  .then(result => {
    if (result === null) {
      return true
    }
    return false
  })
}

/**
 * Set shortlisted products
 */
export const setShortlistedProducts = data => AsyncStorage.setItem('shortlisted', JSON.stringify(data)).then(() => true)
