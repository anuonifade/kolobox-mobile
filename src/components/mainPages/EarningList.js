//This file used for inflate Earning List
import React, { Component } from 'react';
import { ScrollView, View, ActivityIndicator, Text } from 'react-native';
import { connect } from 'react-redux';
import EarningsListRow from './EarningsListRow.js';
import { getUserEarnings } from '../../actions/user-actions';

class EarningList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: '',
      data: []
    };
  }


  componentWillMount = () => {
    const { userData, getUserEarnings } = this.props;
    this.setState({ loading: true });

    getUserEarnings(userData.token)
      .then((earnings) => {
        this.setState({
          loading: false,
          data: earnings.data
        });
      }).catch((error) => {
        this.setState({
          loading: false,
          error: 'There is an error loading the earnings'
        });
        console.log(error);
      });
  }

  renderEarnings = () => {
    if (this.state.error && this.state.error !== '') {
      return (
        <Text style={{ fontWeight: 'bold', top: 10, alignSelf: 'center', alignItems: 'center' }} >
          { this.state.error}
        </Text>
      );
    }
    if (!this.state.data || this.state.data.length === 0) {
      return (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center', top: 20 }}
        >
          <Text style={{ fontWeight: 'bold' }} >
            You do not have any earnings at this time
          </Text>
        </View>
      );
    }
      return (
        this.state.data.map((earning) =>
          (<EarningsListRow data={earning || []} key={earning.id} />)));
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }} >
        { this.state.loading
          ? <ActivityIndicator size="large" color="#1034A6" />
          : this.renderEarnings()
        }
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { userData, earnings } = state.user;
  return {
    userData,
    earnings
  };
};

const mapDispatchToProps = ({
  getUserEarnings
});


export default connect(mapStateToProps, mapDispatchToProps)(EarningList);
