import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';

class ProductCategoryInfo extends Component {

  render() {
    return (
      <View style={{ marginTop: 22 }}>
        <Modal
          animationIn="slideInUp"
          animationOut="slideOutDown"
          backdropOpacity={0}
          backdropColor="white"
          isVisible={this.props.modalVisible}
          onSwipe={() => { this.props.closeModal(false); }}
          swipeDirection="right"
          style={{ padding: 20 }}
        >
          <View style={styles.modalContainer}>
            <View
              style={{
                width: '100%',
                backgroundColor: '#4991bf',
                alignItems: 'center',
                padding: 20,
              }}
            >
              <Text style={{ color: '#fff' }}> { this.props.category.name } </Text>
            </View>
            <View
              style={{
                flex: 1,
                marginTop: 12,
                padding: 5
              }}
            >
              <View style={styles.bodyContainer}>
                <Text style={styles.title}>Benefits</Text>
                <Text>{ this.props.category.benefits}</Text>
              </View>
              <View style={styles.bodyContainer}>
                <Text style={styles.title}>Number of Products: </Text>
                <Text>{ this.props.category.Products.length }</Text>
              </View>
              <View style={styles.bodyContainer}>
                <Text style={styles.title}>Minimum amount: </Text>
                <Text>₦{ this.props.category.minimum_requirement }</Text>
              </View>
              <View style={styles.bodyContainer}>
                <Text style={styles.title}>Interest Rate: </Text>
                <Text>{ this.props.category.interest_rate }%</Text>
              </View>
              <View style={styles.bodyContainer}>
                <Text style={styles.title}>Products: </Text>
                {this.props.category.Products
                  .map((product, index) =>
                    <Text keys={index}>{`${index + 1}. ${product.name}`}</Text>
                  )
                }
              </View>

            </View>
            <View style={{ padding: 20 }}>
              <TouchableOpacity
                style={{
                  height: 50,
                  width: 100,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: '#4991bf'
                }}
                onPress={() => { this.props.closeModal(false); }}
              >
                <Text style={{ color: '#ffffff' }}>CLOSE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = {
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderRadius: 5,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  bodyContainer: {
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    marginBottom: 10,
    marginTop: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: '#000',
    borderTopColor: '#000'
  },
  body: {
    fontWeight: '300',
    fontSize: 14,
    padding: 5
  }
};

export default ProductCategoryInfo;
