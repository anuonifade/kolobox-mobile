//This file used for Earning list detail
import React, { Component } from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import { Actions } from 'react-native-router-flux';
import { AccountHeader } from '../common';
import EarningNameOfDepositTabs from './EarningNameOfDepositTabs';

class EarningNameOfDeposit extends Component {
  //used for footer button
  static navigationOptions = {
    tabBarLabel: 'Earnings',
    tabBarIcon: () => (<Icon size={24} color="white" name="attach-money" />)
  }
  //used for render UI views
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AccountHeader headerTxt={'Kolonaija'} />
        <EarningNameOfDepositTabs />
      </View>
    );
  }
}

export default EarningNameOfDeposit;
