import React, { Component } from 'react';
import Expo from 'expo';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  TextInput,
  WebView,
  Modal
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import RangeBar from '../setupAccount1/Slider';
import { Spinner } from '../common';
import GLOBALS from '../../globals';
import Selector from '../common/selector';

class AccountDetailPage extends Component {
  constructor(props) {
    super(props);
    this.data = null;
    this.state = {
      investmentgoal: null,
      email: this.props.userData.email,
      firstName: this.props.userData.firstname,
      lastName: this.props.userData.lastname,
      value: this.props.product.minimum_amount,
      status: false,
      paying: false,
      authorization_url: '',
      verifying: false,
      product: {},
      isParsed: false,
      reference: '',
      frequency: '',
    };
    this.updateSliderValue = this.updateSliderValue.bind(this);
    this.makePayment = this.makePayment.bind(this);
    this.initializePayment = this.initializePayment.bind(this);
    this.prepareInitialData = this.prepareInitialData.bind(this);
    this.getPaymentDetail = this.getPaymentDetail.bind(this);
  }

  onTextChanged(text) {
    this.setState({ investmentgoal: text });
  }

  setFrequency(text) {
    this.setState({ frequency: text });
  }

  getPaymentDetail(ref) {
    Expo.SecureStore.getItemAsync('PAYSTACK_SECRET_KEY').then((secretKey) => {
      const url = `${GLOBALS.PAYSTACK_URL}/transaction/verify/${ref}`;
      return fetch(url, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${secretKey}`
        }
      }).then(response => response.json()).then((response) => {
        if (response.data.gateway_response === 'Successful' ||
          response.data.gateway_response === 'Approved') {
          Alert.alert('Notice', `Payment ${response.data.gateway_response}`);
          Actions.mainPages();
        } else {
          Alert.alert('Notice ', response.data.gateway_response);
        }
      }).catch(error => {
        console.log('Couldn\'t fetch transaction details', error);
      });
    });
  }

  closeModal() {
    this.setState({ paying: false });
    this.getPaymentDetail(this.state.reference);
  }

  makePayment() {
    Expo.SecureStore.getItemAsync('PAYSTACK_SECRET_KEY').then((secretKey) => {
      this.initializePayment(secretKey);
    });
  }

  prepareInitialData() {
    const metadata = {};
    if (this.props.product.name === 'KOLO-FLEX') {
      metadata.subaccount = 'ACCT_1lnhn6h9dnkivel';
    } else if (this.props.product.name === 'KOLO-TARGET') {
      metadata.subaccount = 'ACCT_j8r3p1iia5hlhmo';
    }
    metadata.user_id = this.props.product.id;
    metadata.type = 'browserClient';
    metadata.product_id = this.props.product.id;
    metadata.product_type = this.props.product.product_type;
    metadata.action = 'productSelection';
    metadata.custom_fields = [
      {
        saving_frequency: this.state.frequency,
        user: `${this.state.firstName} ${this.state.lastName}`,
        product: this.props.product.name
      }
    ];
    return {
      email: this.state.email,
      firstname: this.state.firstName,
      lastname: this.state.lastName,
      amount: this.state.value * 100,
      metadata,
    };
  }

  updateSliderValue(value) {
    this.setState({ value: parseInt(value, 10) });
  }

  initializePayment(secretKey) {
    if (this.state.email === ''
      || this.state.firstName === ''
      || this.state.lastName === ''
      || this.state.value === 0) {
      Alert.alert('Invalid parameters, amount cannot be empty');
    }
    const url = `${GLOBALS.PAYSTACK_URL}transaction/initialize`;
    const data = this.prepareInitialData();

    return fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${secretKey}`
      },
      body: JSON.stringify(data)
    }).then(response => response.json()).then((responseData) => {
      this.setState({
        authorization_url: responseData.data.authorization_url,
        reference: responseData.data.reference,
        paying: true
      });
    }).catch((error) => {
      console.log('Couldnt initialise payment', error);
    });
  }


  renderButton() {
    if (!this.state.paying) {
      return (
        <View style={{ marginTop: 5, alignItems: 'center', justifyContent: 'center', padding: 10 }}>
          <TouchableOpacity
            onPress={() => { this.makePayment(); }}
            style={{ alignItems: 'center', backgroundColor: '#3B90E3', width: 200 }}
          >
            <Text style={Styles.buttonTextStyles}>Make Payment</Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (<Spinner />);
  }

  render() {
    // const routeTo = () => { Actions.mainPages(); };
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <ScrollView style={{ flex: 1 }}>
          <Text
            style={[
              Styles.textStyles,
              { margin: 10 }
            ]}
          >Amount(in ₦) Slide to Increase amount</Text>
          <View style={Styles.RangeBarContainerStyle}>
            <RangeBar
              value={this.props.product.minimum_amount}
              minval={this.props.product.minimum_amount}
              maxval={1000000}
              updateSliderValue={this.updateSliderValue}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              paddingLeft: 20,
              marginTop: 10
            }}
          >
            <Text>Select Deposit Frequency</Text>
            <Selector
              placeholder={'Deposit Frequency'}
              onChange={(text) => this.setFrequency(text)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              paddingLeft: 20,
              marginTop: 10
            }}
          >
            <Text>Investment Goal</Text>
            <TextInput
              keyboardType='numeric'
              style={{
                marginTop: 12,
                paddingRight: 5,
                paddingLeft: 5,
                fontSize: 16,
                paddingBottom: 0,
                height: 25,
                width: 150,
                borderBottomWidth: 2,
                borderColor: '#D3D3D3'
              }}
              onChange={(text) => this.onTextChanged(text)}
              id='investmentgoal'
              value={this.state.myNumber}
            />
          </View>
          <View style={Styles.benefitsContainerStyle}>
            <Text style={Styles.lineStyle} />
            <Text style={Styles.benefitTextStyle}>Make Payment</Text>
            <Text style={Styles.lineStyle} />
          </View>
          {this.renderButton()}
        </ScrollView>
        <View style={{ top: 20 }} >
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.paying}
          >
          { this.state.paying && this.state.authorization_url !== ''
            ? (
                <View style={{ flex: 1, marginTop: 30 }}>
                  <WebView
                    source={{ uri: this.state.authorization_url }}
                    style={{ marginTop: 0 }}
                    onError={() => { this.setState.bind({ paying: false }); }}
                  />
                </View>
            )
            : null
          }
          <View style={{ alignItems: 'center' }}>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: '#3B90E3',
                width: 200
              }}
              onPress={() => {
                this.closeModal();
             }}
            >
             <Text style={Styles.buttonTextStyles}>DONE</Text>
           </TouchableOpacity>
          </View>
          </Modal>
        </View>
      </View>

    );
  }
}

const Styles = {

  textStyles: {
    fontSize: 16,
    color: '#000',
    marginTop: 15,
  },
  buttonTextStyles: {
    fontSize: 16,
    color: '#ffffff',
    marginTop: 15,
    marginBottom: 15,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    marginTop: 20
  },
  RangeBarContainerStyle: {
    flexDirection: 'row',
    marginTop: 5,
    paddingLeft: 5,
    paddingRight: 5
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  benefitTextStyle: {
    fontSize: 16,
    color: '#2b78e4',
    paddingBottom: 10,
    paddingLeft: 5,
    paddingRight: 5,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bulletStyle: {
    fontSize: 27,
    color: '#000000'

  },
  bulletTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 5,
  },
  container: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  selectedPriceTextStyle: {
    borderBottomColor: '#000000',
    borderBottomWidth: 1,
    paddingBottom: 5,
    flex: 1,
    alignItems: 'center'
  },
  pickerContainerStyle: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 30,
    marginRight: 10,
    marginBottom: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerRAdio: {
    flexDirection: 'row'
  },
  text: {
    padding: 10,
    fontSize: 14
  },
  graphTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
    padding: 10
  },
  arrowImageStyle: {
    height: 25,
    width: 25,
    padding: 5,
    justifyContent: 'flex-end'
  },
  debitCardTextStyle: {
    fontSize: 16,
    color: '#000000',
    flex: 1
  }
};

const mapStateToProps = (state) => {
  const { userData } = state.user;
  return {
    userData
  };
};

export default connect(mapStateToProps, null)(AccountDetailPage);
