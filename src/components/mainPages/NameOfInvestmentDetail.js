import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
// import CheckBox from 'react-native-check-box';

class NameOfInvestmentDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: !!props.checked
    };
  }

  componentWillMount() {
  }

  onClick() {
    const { handleCheckToggle, product } = this.props;
    const { checked } = this.state;
    const newCheckedState = !checked;

    handleCheckToggle(product, newCheckedState);
    this.setState({
      checked: newCheckedState
    });
  }

  render() {
    const routeTo = () => {
      const product = this.props.product;
      Actions.InvestmentDetailsPage({ product });
    };

    return (
      <View style={styles.containerStyle}>
        <View style={styles.childContainerStyle}>
          <Text style={styles.textStyles}>{this.props.product.name}</Text>
        </View>
        <View style={styles.interestContainerStyle}>
          <View style={styles.leftViewStyle}>
            <Text style={styles.blackTextStyles}>INTEREST</Text>
            <Text style={styles.blackTextStyles}>{this.props.product.interest_rate}</Text>
          </View>
          <View style={styles.rightViewStyle}>
            <Text style={styles.blackTextStyles}>Month</Text>
            <Text style={styles.blackTextStyles}>
              {this.props.product.tenor}
            </Text>
          </View>

        </View>

        <View style={styles.accountContainerStyle}>
          <View style={styles.leftViewStyle}>
            <Text style={styles.blackTextStyles}>Amount</Text>
            <Text style={styles.blackTextStyles}>₦{this.props.product.minimum_amount}</Text>
          </View>
          <View style={styles.rightViewStyle}>
            <Text style={styles.blackTextStyles}>Tenure</Text>
            <Text style={styles.blackTextStyles}>{this.props.product.tenor || 'N.A'}</Text>
          </View>
        </View>
        <View>
          <TouchableOpacity
            onPress={routeTo.bind(this)}
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#4991bf',
              paddingTop: 10,
              paddingBottom: 10,
            }}
          >
            <Text style={styles.textStyles}>CONTINUE >> </Text>
          </TouchableOpacity>
        </View>
      </View>

    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    borderColor: '#4991bf',
    borderWidth: 2,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    position: 'relative'
  },
  childContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: '#4991bf',
    padding: 5
  },
  checkBoxImage: {
    height: 25,
    width: 25,
    padding: 5,
    marginTop: 5
  },
  textStyles: {
    fontSize: 16,
    color: '#ffffff',
    padding: 5,
    flex: 1,
    justifyContent: 'flex-start',
  },
  blackTextStyles: {
    fontSize: 16,
    color: '#000000',
    padding: 5,
  },
  interestContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#4991bf',
    padding: 5
  },
  accountContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 5
  },
  rightViewStyle: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  leftViewStyle: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  arrowImageView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row'
  }
};

export default NameOfInvestmentDetail;
