import React, { Component } from 'react'
import { View, Text, Alert, AsyncStorage } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Input, FullWidthButton, Header, Spinner } from '../common'
import GLOBALS from '../../globals'


export default class ForgotPassword extends Component {
  constructor() {
    super()
    this.state = {
      emailphone: '',
      submit: false
    }
  }
  componentWillUnmount() {
    this.setState({ submit: false })
  }
  forgotpassApi() {
    this.setState({ submit: true })
    AsyncStorage.setItem('fromforgot', 'true')
    fetch(`${GLOBALS.BASE_URL}auth/user/forget_password`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        emailPhone: this.state.emailphone
      })
    }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.status === true) {
          AsyncStorage.setItem('forgotpasstoken', JSON.stringify(responseJson.data))
          this.setState({ submit: false })
          // console.log('hello')
          Alert.alert(
            'Success',
            responseJson.data.message,
            [
              { text: 'OK', onPress: () => Actions.ValidateCode() }
            ],
            { cancelable: false }
          )
        } else {
          this.setState({ submit: false })
          Alert.alert(
            'Error!',
            responseJson.message,
            [
              { text: 'OK' }
            ],
            { cancelable: false }
          )
        }
      })
      .catch((error) => {
        this.setState({ submit: false })
        console.error(error)
      })
  }
  renderButton() {
    if (!this.state.submit) {
      return (
        <FullWidthButton
          onPress={this.forgotpassApi.bind(this) }
          Title={'Submit'}
          disabled={!this.state.submit }
        />
      )
    }
      return (<Spinner />)
  }
  render() {
    // const routeTo = () => { Actions.verifyPin(); }
    const { cover, TextLabel, forgotButton } = Styles
    return (
      <View style={cover} >
        <Header />
        <View style={forgotButton}>
          <Text style={TextLabel}>
            Forgot Password
          </Text>
        </View>
        <View style={{ paddingLeft: 8, paddingRight: 8 }}>
          <View style={{ height: 50 }}>
            <Input
              onChangeText={emailphone => this.setState({ emailphone })}
              label={'envelope'} placeholder={'Email/Phone'}
            />
          </View>
          <View style={{ marginTop: 30, height: 40 }}>
            {this.renderButton()}
          </View>
        </View>
      </View>
    )
  }
}
const Styles = {
  cover: {
    flex: 1,
    flexDirection: 'column'
  },
  forgotButton: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    marginTop: 5,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd'
  },

  TextLabel: {
    fontSize: 18,
    color: '#000000',
    fontWeight: '100'

  }
}
