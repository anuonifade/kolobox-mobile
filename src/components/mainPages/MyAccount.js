//This file used for My Account. All the account UI rendered through it.
import React, { Component } from 'react'
import { View } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

// import { Actions } from 'react-native-router-flux'
import { AccountHeader } from '../common'
import MyAccountTabs from './MyAccountTabs'


class MyAccount extends Component {
  static navigationOptions = {
    tabBarLabel: 'Account',
    tabBarIcon: () => (<Icon size={24} color="white" name="person" />)
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <AccountHeader showHelp showWallet headerTxt={'Kolobox'} />
        <MyAccountTabs />
      </View>
    )
  }
}

export default MyAccount
