import React, { Component } from 'react'
import { View, TextInput, Image } from 'react-native'
import ModalSelector from 'react-native-modal-selector'
import Icon from 'react-native-vector-icons/FontAwesome'
import Moment from 'moment'

export default class YearSelector extends Component {
  constructor (props) {
    super(props)
    this.state = {
      year: ''
    }
  }

  render () {
    let index = 0
    const dt = new Date()
    const year = parseInt(Moment(dt).format('YYYY'))
    const data = [
            { key: index++, section: true, label: 'Year' }
    ]

    for (let i = 0; i < 20; i++) {
      const yr = year + i
      const ob = { key: yr, label: yr.toString() }
      data.push(ob)
    }

    return (
      <View style={{ flexDirection: 'column' }}>
        <ModalSelector
          data={data}
          initValue='01'
          supportedOrientations={['portrait']}
          onChange={(option) => { this.setState({ year: option.label }) }}>
          <View style={{ flexDirection: 'row' }}>
            <Icon name={'credit-card'} size={24} color='grey' style={{ padding: 2 }} />
            <TextInput
              style={{ borderBottomWidth: 2, borderColor: '#D3D3D3', width: 80, marginLeft: 10, height: 30, fontSize: 14, alignSelf: 'center' }}
              editable={false}
              placeholder='Year'
              value={this.state.year}
                />
            <Image style={Styles.arrowImageStyle} source={require('../img/down_arrow.png')} />
          </View>
        </ModalSelector>

      </View>
    )
  }
}

const Styles = {
  arrowImageStyle: {
    height: 25,
    width: 25,
    marginTop: 5,
    marginRight: 5
  }
}
