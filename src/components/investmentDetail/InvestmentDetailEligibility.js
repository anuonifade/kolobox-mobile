import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'

class InvestmentDetailEligibility extends Component {
  render () {
    const { product } = this.props

    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-start' }}>
        <ScrollView>
          <Text style={Styles.textStyles} >{product.eligibility}</Text>
        </ScrollView>
      </View>

    )
  }
}

const Styles = {

  textStyles: {
    fontSize: 16,
    color: '#000000',
    margin: 10,
  },
  benefitsContainerStyle: {
    flexDirection: 'row',
    margin: 10
  },
  lineStyle: {
    height: 2,
    backgroundColor: '#2b78e4',
    flex: 1,
    alignItems: 'center'
  },
  benefitTextStyle: {
    flex: 1,
    fontSize: 16,
    color: '#2b78e4',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bulletStyle: {
    fontSize: 27,
    color: '#000000'

  },
  bulletTextStyle: {
    fontSize: 16,
    color: '#000000',
    margin: 5,
  }
};

export default InvestmentDetailEligibility;
