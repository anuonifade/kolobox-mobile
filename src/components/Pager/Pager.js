/* {all pager render in this screen} */
import React from 'react'

import { View } from 'react-native'

import PageSwiper from './ViewPager'

const Pager = () => {
  return <View style={{ flex: 1 }}>
    <View style={{ flex: 1, padding: 25, marginTop: 20 }} />
    <View style={{ flex: 15, flexDirection: 'column', justifyContent: 'flex-start' }}>
      <PageSwiper />
    </View>
  </View>
}

export default Pager
