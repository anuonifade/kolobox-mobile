import { ADD_TOAST } from '../constants';

export function addToast (message) {
  return { type: ADD_TOAST, message }
}
